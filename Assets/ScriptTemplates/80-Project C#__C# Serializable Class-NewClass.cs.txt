/*
 *  Description : #PROJECTNAME#
 *
 *  Author ： #AUTHOR#
 *
 *  Date ： #CREATEDATE#
 */

using System;

namespace #PROJECTNAME#
{
    [Serializable]
    public class #SCRIPTNAME#
    {
        public #SCRIPTNAME#() { }
    }
}