using System;
using UnityEditor;
using UnityEngine;

internal sealed class ScriptKeywordProcessor : UnityEditor.AssetModificationProcessor
{
    public static void OnWillCreateAsset(string path)
    {
        path = path.Replace(".meta", "");
        int index = path.LastIndexOf(".");
        if (index < 0)
            return;

        string file = path.Substring(index);
        if (file != ".cs" && file != ".js")
            return;

        index = Application.dataPath.LastIndexOf("Assets");
        path = Application.dataPath.Substring(0, index) + path;
        if (!System.IO.File.Exists(path))
            return;

        DeveloperInformation information = AssetDatabase.LoadAssetAtPath<DeveloperInformation>("Assets/ScriptTemplates/DeveloperInformation.asset");

        string fileContent = System.IO.File.ReadAllText(path);
        fileContent = fileContent.Replace("#PROJECTNAME#", Application.productName);
        fileContent = fileContent.Replace("#CREATEDATE#", DateTime.Now.ToString("yyyy/MM/dd"));
        if (information)
        {
            fileContent = fileContent.Replace("#AUTHOR#", information.Author);
        }
        else
        {
            Debug.LogError("Failed to load DeveloperInformation");
        }

        System.IO.File.WriteAllText(path, fileContent);
        AssetDatabase.Refresh();
    }
}