using UnityEngine;

[CreateAssetMenu(fileName = "DeveloperInformation", menuName = "DeveloperInformation", order = 0)]
public class DeveloperInformation : ScriptableObject
{
    [SerializeField]
    private string author;

    public string Author
    {
        get
        {
            return author;
        }
    }
}