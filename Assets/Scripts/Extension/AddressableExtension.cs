/*
 *  Project Name：Utility.Extensions
 *
 *  Programmer：Nick Tseng
 *
 *  Start Date：2022/09/12
 *  
 *  UnityVersion：2020.3.15f2
 */

using System;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceLocations;

namespace Utility.Extensions
{
    public static class AddressableExtension
    {
        /// <summary>
        /// Addresable 資源驗證
        /// </summary>
        /// <param name="key"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool AddressableResourceExists(object key, Type type)
        {
            foreach (var l in Addressables.ResourceLocators)
            {
                IList<IResourceLocation> locs;
                if (l.Locate(key, type, out locs))
                    return true;
            }
            return false;
        }
    }
}