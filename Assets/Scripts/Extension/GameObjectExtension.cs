using System.Collections.Generic;
using UnityEngine;
namespace Utility.Extensions
{
    public static partial class ExtensionMethods
    {
        #region ObjectNull
        public static bool IsNull<T>(this T objects, string message = "") where T : class
        {
            if (objects is UnityEngine.Object obj)
            {
                if (!obj)
                {
                    if (message != "")
                        Debug.LogErrorFormat("The object is NULL! {0}", message);

                    return true;
                }
            }
            else
            {
                if (objects == null)
                {
                    if (message != "")
                        Debug.LogErrorFormat("The object is NULL! {0}", message);

                    return true;
                }
            }

            return false;
        }
        #endregion

        #region GameObject-GetOrAddComponent
        /// <summary>
        /// 如果有找到Component則回傳，反之則新增Component。
        /// </summary>
        public static T GetOrAddComponent<T>(this GameObject gameObjects) where T : Component
        {
            return gameObjects.GetComponent<T>() ?? gameObjects.AddComponent<T>();
        }
        #endregion

        #region GameObject-GetComponent
        private static List<Component> _componentCache = new List<Component>();
        /// <param name="thisObject"></param>
        /// <param name="componentType"></param>
        /// <returns></returns>
		public static Component GetComponentNoAlloc(this GameObject thisObject, System.Type componentType)
        {
            thisObject.GetComponents(componentType, _componentCache);
            Component component = _componentCache.Count > 0 ? _componentCache[0] : null;
            _componentCache.Clear();
            return component;
        }
        /// <typeparam name="T"></typeparam>
        /// <param name="thisObject"></param>
        /// <returns></returns>
        public static T GetComponentNoAlloc<T>(this GameObject thisObject) where T : Component
        {
            thisObject.GetComponents(typeof(T), _componentCache);
            Component component = _componentCache.Count > 0 ? _componentCache[0] : null;
            _componentCache.Clear();
            return component as T;
        }

        #endregion
    }
}
