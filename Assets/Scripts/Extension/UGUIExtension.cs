﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
namespace Utility.Extensions
{
    public static partial class ExtensionMethods
    {
        #region ToggleGroup
        private static FieldInfo toggleList;
        /// <summary>
        /// 從ToggleGroup取得Toggle列表
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static IList<Toggle> GetToggles(this ToggleGroup target)
        {
            if (toggleList == null)
            {
                toggleList = typeof(ToggleGroup).GetField("m_Toggles", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                if (toggleList == null)
                    throw new System.Exception("m_toggleList is NULL!!");
            }
            return toggleList.GetValue(target) as IList<Toggle>;
        }

        public static int Count(this ToggleGroup target)
        {
            return GetToggles(target).Count;
        }

        public static Toggle Get(this ToggleGroup target, int index)
        {
            return GetToggles(target)[index];
        }
        #endregion

        #region RectTransform-Align
        public static void TopLeft(this RectTransform rect, bool adjustPivot = true)
        {
            rect.SetAlignment(Vector2.up, adjustPivot);
        }
        public static void TopLeft(this RectTransform rect, bool adjustPivot, Vector2 offset, Vector2 size)
        {
            rect.TopLeft(adjustPivot);
            rect.anchoredPosition = offset;
            rect.sizeDelta = size;
        }
        public static void Top(this RectTransform rect, bool adjustPivot = true)
        {
            rect.SetAlignment(new Vector2(0.5f, 1f), adjustPivot);
        }
        public static void Top(this RectTransform rect, bool adjustPivot, Vector2 offset, Vector2 size)
        {
            rect.Top(adjustPivot);
            rect.anchoredPosition = offset;
            rect.sizeDelta = size;
        }
        public static void TopRight(this RectTransform rect, bool adjustPivot = true)
        {
            rect.SetAlignment(Vector2.one, adjustPivot);
        }
        public static void TopRight(this RectTransform rect, bool adjustPivot, Vector2 offset, Vector2 size)
        {
            rect.TopRight(adjustPivot);
            rect.anchoredPosition = offset;
            rect.sizeDelta = size;
        }
        public static void Left(this RectTransform rect, bool adjustPivot = true)
        {
            rect.SetAlignment(new Vector2(0f, 0.5f), adjustPivot);
        }
        public static void Left(this RectTransform rect, bool adjustPivot, Vector2 offset, Vector2 size)
        {
            rect.Left(adjustPivot);
            rect.anchoredPosition = offset;
            rect.sizeDelta = size;
        }
        public static void Center(this RectTransform rect, bool adjustPivot = true)
        {
            rect.SetAlignment(new Vector2(0.5f, 0.5f), adjustPivot);
        }
        public static void Center(this RectTransform rect, bool adjustPivot, Vector2 offset, Vector2 size)
        {
            rect.Center(adjustPivot);
            rect.anchoredPosition = offset;
            rect.sizeDelta = size;
        }
        public static void Right(this RectTransform rect, bool adjustPivot = true)
        {
            rect.SetAlignment(new Vector2(1f, 0.5f), adjustPivot);
        }
        public static void Right(this RectTransform rect, bool adjustPivot, Vector2 offset, Vector2 size)
        {
            rect.Right(adjustPivot);
            rect.anchoredPosition = offset;
            rect.sizeDelta = size;
        }
        public static void BottomLeft(this RectTransform rect, bool adjustPivot = true)
        {
            rect.SetAlignment(Vector2.zero, adjustPivot);
        }
        public static void BottomLeft(this RectTransform rect, bool adjustPivot, Vector2 offset, Vector2 size)
        {
            rect.BottomLeft(adjustPivot);
            rect.anchoredPosition = offset;
            rect.sizeDelta = size;
        }
        public static void Bottom(this RectTransform rect, bool adjustPivot = true)
        {
            rect.SetAlignment(new Vector2(0.5f, 0f), adjustPivot);
        }
        public static void Bottom(this RectTransform rect, bool adjustPivot, Vector2 offset, Vector2 size)
        {
            rect.Bottom(adjustPivot);
            rect.anchoredPosition = offset;
            rect.sizeDelta = size;
        }
        public static void BottomRight(this RectTransform rect, bool adjustPivot = true)
        {
            rect.SetAlignment(Vector2.right, adjustPivot);
        }
        public static void BottomRight(this RectTransform rect, bool adjustPivot, Vector2 offset, Vector2 size)
        {
            rect.BottomRight(adjustPivot);
            rect.anchoredPosition = offset;
            rect.sizeDelta = size;
        }
        public static void Align(this RectTransform rect, AlignmentRect alignment, bool adjustPivot = true)
        {
            switch (alignment)
            {
                case AlignmentRect.TopLeft:
                    rect.TopLeft(adjustPivot);
                    break;
                case AlignmentRect.Top:
                    rect.Top(adjustPivot);
                    break;
                case AlignmentRect.TopRight:
                    rect.TopRight(adjustPivot);
                    break;
                case AlignmentRect.Left:
                    rect.Left(adjustPivot);
                    break;
                case AlignmentRect.Center:
                    rect.Center(adjustPivot);
                    break;
                case AlignmentRect.Right:
                    rect.Right(adjustPivot);
                    break;
                case AlignmentRect.BottomLeft:
                    rect.BottomLeft(adjustPivot);
                    break;
                case AlignmentRect.Bottom:
                    rect.Bottom(adjustPivot);
                    break;
                case AlignmentRect.BottomRight:
                    rect.BottomRight(adjustPivot);
                    break;
            }
        }
        public static void Align(this RectTransform rect, AlignmentRect alignment, bool adjustPivot, Vector2 offset, Vector2 size)
        {
            switch (alignment)
            {
                case AlignmentRect.TopLeft:
                    rect.TopLeft(adjustPivot, offset, size);
                    break;
                case AlignmentRect.Top:
                    rect.Top(adjustPivot, offset, size);
                    break;
                case AlignmentRect.TopRight:
                    rect.TopRight(adjustPivot, offset, size);
                    break;
                case AlignmentRect.Left:
                    rect.Left(adjustPivot, offset, size);
                    break;
                case AlignmentRect.Center:
                    rect.Center(adjustPivot, offset, size);
                    break;
                case AlignmentRect.Right:
                    rect.Right(adjustPivot, offset, size);
                    break;
                case AlignmentRect.BottomLeft:
                    rect.BottomLeft(adjustPivot, offset, size);
                    break;
                case AlignmentRect.Bottom:
                    rect.Bottom(adjustPivot, offset, size);
                    break;
                case AlignmentRect.BottomRight:
                    rect.BottomRight(adjustPivot, offset, size);
                    break;
            }
        }
        public enum AlignmentRect
        {
            TopLeft = 0,
            Top,
            TopRight,
            Left,
            Center,
            Right,
            BottomLeft,
            Bottom,
            BottomRight
        }
        static void SetAlignment(this RectTransform rect, Vector2 value, bool adjustPivot)
        {
            rect.anchorMax = value;
            rect.anchorMin = value;
            if (adjustPivot) rect.pivot = value;
        }
        #endregion

        #region RectTransform-Stretch
        public static void StretchHorizontalTop(this RectTransform rect, float paddingLeft, float offsetY,
            float paddingRight, float height, bool adjustPivot)
        {
            rect.anchorMin = Vector2.up;
            rect.anchorMax = Vector2.one;
            if (adjustPivot)
            {
                rect.pivot = new Vector2(rect.pivot.x, 1f);
            }
            rect.SetStretchHorizontalRect(paddingLeft, offsetY, paddingRight, height);
        }
        public static void StretchHorizontalCenter(this RectTransform rect, float paddingLeft, float offsetY,
            float paddingRight, float height, bool adjustPivot)
        {
            rect.anchorMin = new Vector2(0f, 0.5f);
            rect.anchorMax = new Vector2(1f, 0.5f);
            if (adjustPivot)
            {
                rect.pivot = new Vector2(rect.pivot.x, 0.5f);
            }
            rect.SetStretchHorizontalRect(paddingLeft, offsetY, paddingRight, height);
        }
        public static void StretchHorizontalBottom(this RectTransform rect, float paddingLeft, float offsetY,
            float paddingRight, float height, bool adjustPivot)
        {
            rect.anchorMin = Vector2.zero;
            rect.anchorMax = Vector2.right;
            if (adjustPivot)
            {
                rect.pivot = new Vector2(rect.pivot.x, 0f);
            }
            rect.SetStretchHorizontalRect(paddingLeft, offsetY, paddingRight, height);
        }
        public static void StretchVerticalLeft(this RectTransform rect, float offsetX, float paddingTop,
            float width, float paddingBottom, bool adjustPivot)
        {
            rect.anchorMin = Vector2.zero;
            rect.anchorMax = Vector2.up;
            if (adjustPivot)
            {
                rect.pivot = new Vector2(0f, rect.pivot.y);
            }
            rect.SetStretchVerticalRect(offsetX, paddingTop, width, paddingBottom);
        }
        public static void StretchVerticalCenter(this RectTransform rect, float offsetX, float paddingTop,
            float width, float paddingBottom, bool adjustPivot)
        {
            rect.anchorMin = new Vector2(0.5f, 0f);
            rect.anchorMax = new Vector2(0.5f, 1f);
            if (adjustPivot)
            {
                rect.pivot = new Vector2(0.5f, rect.pivot.y);
            }
            rect.SetStretchVerticalRect(offsetX, paddingTop, width, paddingBottom);
        }
        public static void StretchVerticalRight(this RectTransform rect, float offsetX, float paddingTop,
            float width, float paddingBottom, bool adjustPivot)
        {
            rect.anchorMin = Vector2.right;
            rect.anchorMax = Vector2.one;
            if (adjustPivot)
            {
                rect.pivot = new Vector2(1f, rect.pivot.y);
            }
            rect.SetStretchVerticalRect(offsetX, paddingTop, width, paddingBottom);
        }
        public static void StretchFull(this RectTransform rect, float paddingLeft, float paddingTop,
            float paddingRight, float paddingBottom)
        {
            rect.anchorMin = Vector2.zero;
            rect.anchorMax = Vector2.one;
            rect.offsetMin = new Vector2(paddingLeft, paddingBottom);
            rect.offsetMax = new Vector2(-paddingRight, -paddingTop);
        }
        public static void Stretch(this RectTransform rect, StretchRect stretch, float leftOrOffsetX,
            float topOrOffsetY, float rightOrWidth, float bottomOrHeight, bool adjustPivot)
        {
            switch (stretch)
            {
                case StretchRect.HorizontalTop:
                    rect.StretchHorizontalTop(leftOrOffsetX, topOrOffsetY, rightOrWidth, bottomOrHeight, adjustPivot);
                    break;
                case StretchRect.HorizontalCenter:
                    rect.StretchHorizontalCenter(leftOrOffsetX, topOrOffsetY, rightOrWidth, bottomOrHeight, adjustPivot);
                    break;
                case StretchRect.HorizontalBottom:
                    rect.StretchHorizontalBottom(leftOrOffsetX, topOrOffsetY, rightOrWidth, bottomOrHeight, adjustPivot);
                    break;
                case StretchRect.VerticalLeft:
                    rect.StretchVerticalLeft(leftOrOffsetX, topOrOffsetY, rightOrWidth, bottomOrHeight, adjustPivot);
                    break;
                case StretchRect.VerticalCenter:
                    rect.StretchVerticalCenter(leftOrOffsetX, topOrOffsetY, rightOrWidth, bottomOrHeight, adjustPivot);
                    break;
                case StretchRect.VerticalRight:
                    rect.StretchVerticalRight(leftOrOffsetX, topOrOffsetY, rightOrWidth, bottomOrHeight, adjustPivot);
                    break;
                case StretchRect.FullStretch:
                    rect.StretchFull(leftOrOffsetX, topOrOffsetY, rightOrWidth, bottomOrHeight);
                    break;
            }
        }
        public enum StretchRect
        {
            HorizontalTop,
            HorizontalCenter,
            HorizontalBottom,
            VerticalLeft,
            VerticalCenter,
            VerticalRight,
            FullStretch
        }
        static void SetStretchHorizontalRect(this RectTransform rect, float paddingLeft, float offsetY, float paddingRight, float height)
        {
            rect.offsetMin = new Vector2(paddingLeft, rect.offsetMin.y);
            rect.offsetMax = new Vector2(-paddingRight, rect.offsetMax.y);
            rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, offsetY);
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, height);
        }
        static void SetStretchVerticalRect(this RectTransform rect, float offsetX, float paddingTop, float width, float paddingBottom)
        {
            rect.offsetMin = new Vector2(rect.offsetMin.x, paddingBottom);
            rect.offsetMax = new Vector2(rect.offsetMax.y, -paddingTop);
            rect.anchoredPosition = new Vector2(offsetX, rect.anchoredPosition.y);
            rect.sizeDelta = new Vector2(width, rect.sizeDelta.y);
        }
        #endregion
    }
}
