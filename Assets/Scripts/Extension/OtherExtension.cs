using System;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Utility.Extensions
{
    public static partial class ExtensionMethods
    {
        #region String
        /// <summary>
        /// 判斷字串
        /// </summary>
        public static bool IsNullorEmpty(this string str)
        {
            return (str == null || str == string.Empty) ? true : false;
        }
        /// <summary>
        /// RichText-粗體
        /// </summary>
        /// <param name="str">內容</param>
        /// <returns>粗體文字</returns>
        public static string Bold(this string str) => $"<b>{str}</b>";
        /// <summary>
        /// RichText-顏色
        /// </summary>
        /// <param name="str">內容</param>
        /// <param name="color">色碼</param>
        /// <returns>顏色文字</returns>
        public static string Color(this string str, string color) => $"<color={color}>{str}</color>";
        /// <summary>
        /// RichText-顏色
        /// </summary>
        /// <param name="str">內容</param>
        /// <param name="color">顏色</param>
        /// <returns>顏色文字</returns>
        public static string Color(this string str, Color color) => $"<color={ToRGBHex(color)}>{str}</color>";
        /// <summary>
        /// RichText-斜體
        /// </summary>
        /// <param name="str">內容</param>
        /// <returns>斜體文字</returns>
        public static string Italic(this string str) => $"<i>{str}</i>";
        /// <summary>
        /// RichText-尺寸
        /// </summary>
        /// <param name="str">內容</param>
        /// <param name="size">字級大小</param>
        /// <returns></returns>
        public static string Size(this string str, int size) => $"<size={size}>{str}</size>";
        /// <summary>
        /// Color轉色碼
        /// </summary>
        /// <param name="color">顏色</param>
        /// <returns></returns>
        public static string ToRGBHex(Color color)
        {
            return string.Format("#{0:X2}{1:X2}{2:X2}", ColorToByte(color.r), ColorToByte(color.g), ColorToByte(color.b));
        }
        private static byte ColorToByte(float f)
        {
            f = Mathf.Clamp01(f);
            return (byte)(f * 255);
        }
        #endregion
        #region Regex
        public static string RegexNumber(this string str)
        {
            try
            {
                return Regex.Replace(str, @"[^\d]", "", RegexOptions.None, TimeSpan.FromSeconds(1.5f));
            }

            catch (RegexMatchTimeoutException)
            {
                return string.Empty;
            }
        }
        public static string RegexPW(this string str)
        {
            try
            {
                return Regex.Replace(str, @"[^\w\-+*/]", "", RegexOptions.None, TimeSpan.FromSeconds(1.5f));
            }

            catch (RegexMatchTimeoutException)
            {
                return string.Empty;
            }
        }
        #endregion
    }
}