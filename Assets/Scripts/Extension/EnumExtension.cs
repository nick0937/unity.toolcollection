using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace Utility.Extensions
{
    public static partial class ExtensionMethods
    {
        /// <summary>
        /// 取得列舉的敘述
        /// </summary>
        /// <param name="enumObj">列舉物件</param>
        /// <returns>列舉敘述</returns>
        public static string GetDescription(this Enum enumObj)
        {
            FieldInfo fieldInfo = enumObj.GetType().GetField(enumObj.ToString());

            object[] attribArray = fieldInfo.GetCustomAttributes(false);

            if (attribArray.Length == 0)
            {
                return enumObj.ToString();
            }
            else
            {
                foreach (var attrib in attribArray)
                {
                    if (attrib is DescriptionAttribute targetAttribute)
                    {
                        return targetAttribute.Description;
                    }
                }
                return enumObj.ToString();
            }
        }
        public static T GetDescription<T>(this Enum enumObj)
        {
            FieldInfo fieldInfo = enumObj.GetType().GetField(enumObj.ToString());

            object[] attribArray = fieldInfo.GetCustomAttributes(false);

            foreach (var attrib in attribArray)
            {
                if (attrib is T targetAttribute)
                {
                    return targetAttribute;
                }
            }
            return default;
        }
        /// <summary>
        /// 取得列舉與其敘述的Dictionary [Enum, Enum.Description]
        /// </summary>
        /// <typeparam name="TEnum">列舉型別</typeparam>
        /// <returns>列舉與其敘述的Dictionary</returns>
        public static Dictionary<TEnum, string> GetDescriptionToDic<TEnum>() where TEnum : Enum
        {

            Dictionary<TEnum, string> descriptionDic = new Dictionary<TEnum, string>();

            foreach (TEnum enumValue in Enum.GetValues(typeof(TEnum)))
            {
                descriptionDic.Add(enumValue, (enumValue as Enum).GetDescription());
            }

            return descriptionDic;
        }
        /// <summary>
        /// 取得列舉
        /// </summary>
        /// <param name="description">列舉型別</paramsz>
        public static T GetValueFromDescription<T>(string description) where T : Enum
        {
            foreach (var field in typeof(T).GetFields())
            {
                if (Attribute.GetCustomAttribute(field,
                typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
                {
                    if (attribute.Description == description)
                        return (T)field.GetValue(null);
                }
                else
                {
                    if (field.Name == description)
                        return (T)field.GetValue(null);
                }
            }
            return default;
        }
    }
}