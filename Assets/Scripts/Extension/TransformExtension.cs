using UnityEngine;
namespace Utility.Extensions
{
    public static partial class ExtensionMethods
    {
        #region GetOrAddComponent
        /// <summary>
        /// 如果有找到Component則回傳，反之則新增Component。
        /// </summary>
        public static T GetOrAddComponent<T>(this Transform transforms) where T : Component
        {
            return transforms.GetComponent<T>() ?? transforms.gameObject.AddComponent<T>();
        }
        #endregion
    }
}
