﻿using NickABC.Utils;
using NickABC.Utils.Other;
using System;
using System.Collections;
using UnityEngine;

namespace Utility.Extensions
{
    public static partial class ExtensionMethods
    {
        #region CoroutineGo
        public static CoroutineMGR CoroutineGo(this MonoBehaviour monoBehaviour)
        {
            return CoroutineMGR.CoroutineMGR_Pool.Spawn(monoBehaviour);
        }
        #endregion
        static Pool<CoroutineWaitForSeconds> poolWaitForSeconds;
        public static Pool<CoroutineWaitForSeconds> PoolWaitForSeconds
        {
            get
            {
                if (poolWaitForSeconds.IsNull())
                {
                    poolWaitForSeconds = new Pool<CoroutineWaitForSeconds>();
                }
                return poolWaitForSeconds;
            }
        }
        #region Situation
        public static IEnumerator WaitForSeconds(Action callback, float time)
        {
            CoroutineWaitForSeconds waitForSeconds = PoolWaitForSeconds.Spawn();
            yield return waitForSeconds.Wait(time);
            callback();
            PoolWaitForSeconds.Despawn(waitForSeconds);
        }

        public static IEnumerator WaitForEndOfFrame(Action callback, int frame)
        {
            while (frame > 0)
            {
                yield return null;
                frame--;
            }
            callback();
        }

        public static IEnumerator WaitForFlag(Action callback, Func<bool> waitFlag)
        {
            while (!waitFlag())
            {
                yield return null;
            }
            callback.Invoke();
        }

        public static IEnumerator LoopAction(Action callback, Func<bool> stopLoop)
        {
            while (!stopLoop())
            {
                callback.Invoke();
                yield return null;
            }
        }
        public static IEnumerator IntervalLoopAction(Action callback, float time, Func<bool> stopLoop)
        {
            CoroutineWaitForSeconds waitForSeconds = PoolWaitForSeconds.Spawn();
            while (!stopLoop())
            {
                callback.Invoke();
                yield return waitForSeconds.Wait(time);
            }
        }
        #endregion
    }
}
