/*
 *  Description : TongitsTime
 *
 *  Author ： Nick Tseng
 *
 *  Date ： 2022/12/29
 */

using System.Threading;

namespace Utility
{
    public static class UniTaskExtension
    {
        #region Variable

        #endregion
        #region Public Method
        public static CancellationToken RefreshToken(ref CancellationTokenSource tokenSource)
        {
            if (!tokenSource.IsCancellationRequested) tokenSource?.Cancel();
            tokenSource?.Dispose();
            tokenSource = new CancellationTokenSource();
            return tokenSource.Token;
        }
        #endregion
        #region Protected Method

        #endregion
        #region Private Method

        #endregion
    }
}