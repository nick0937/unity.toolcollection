﻿using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using Utility.Other;
using Utility.Singleton;

namespace Public.Tools
{
    public class SoundMGR : MonoSingleton<SoundMGR>
    {
        /// <suary>
        /// 播放軌道
        /// </suary>
        public enum SoundManagerTracks { Master, Music, Sfx, UI }

        [Header("Settings")]
        [Tooltip("當前設定")]
        public SoundMGRSettingsSO settingsSo;

        [Header("Pool")]
        [Tooltip("AudioSource Pool 數量")]
        public int AudioSourcePoolSize = 10;

        protected GameObject AudioSourceItem;
        protected ObjectPool<AudioSource> soundPool;
        protected SoundMGRSound sound;
        protected List<SoundMGRSound> sounds;
        #region Public Method
        #region PlaySound
        public virtual AudioSource PlaySound(AudioClip audioClip, SoundMGRPlayOptions options)
        {
            return PlaySound(audioClip, options.ESoundManagerTrack,
                options.BypassEffects, options.BypassListenerEffects, options.BypassReverbZones,
                options.Loop, options.Priority, options.Volume, options.Pitch,
                options.ID, options.Persistent, options.AudioGroup,
                options.SoloSingleTrack, options.SoloAllTracks, options.AutoUnSoloOnEnd
            );
        }
        public virtual AudioSource PlaySound(AudioClip audioClip, SoundManagerTracks soundManagerTrack,
                                            bool bypassEffects = false, bool bypassListenerEffects = false, bool bypassReverbZones = false,
                                            bool loop = false, int priority = 128, float volume = 1.0f, float pitch = 1f,
                                            int ID = 0, bool persistent = false, AudioMixerGroup audioGroup = null,
                                            bool soloSingleTrack = false, bool soloAllTracks = false, bool autoUnSoloOnEnd = false)
        {
            if (!audioClip) { return null; }

            AudioSource audioSource = null;

            for (int i = 0; i < sounds.Count; i++)
            {
                if (sounds[i].Source != null && sounds[i].Source.isActiveAndEnabled)
                {
                    if (sounds[i].Source.clip == audioClip)
                    {
                        audioSource = sounds[i].Source;
                        break;
                    }
                }
            }
            if (audioSource == null)
            {
                audioSource = soundPool.Spawn();
            }
            //如果不是重複播放，則播放完後自動禁用
            if ((audioSource != null) && (!loop))
            {
                AutoDisableAudioSource(audioClip.length / Mathf.Abs(pitch), audioSource, audioClip);
            }
            //套用 audio source 設定
            audioSource.time = 0.0f;
            audioSource.clip = audioClip;
            audioSource.pitch = pitch;
            audioSource.loop = loop;
            audioSource.bypassEffects = bypassEffects;
            audioSource.bypassListenerEffects = bypassListenerEffects;
            audioSource.bypassReverbZones = bypassReverbZones;
            audioSource.priority = priority;
            // 音軌
            if (settingsSo != null)
            {
                audioSource.outputAudioMixerGroup = settingsSo.MasterAudioMixerGroup;
                switch (soundManagerTrack)
                {
                    case SoundManagerTracks.Master:
                        audioSource.outputAudioMixerGroup = settingsSo.MasterAudioMixerGroup;
                        break;
                    case SoundManagerTracks.Music:
                        audioSource.outputAudioMixerGroup = settingsSo.MusicAudioMixerGroup;
                        break;
                    case SoundManagerTracks.Sfx:
                        audioSource.outputAudioMixerGroup = settingsSo.SfxAudioMixerGroup;
                        break;
                    case SoundManagerTracks.UI:
                        audioSource.outputAudioMixerGroup = settingsSo.UIAudioMixerGroup;
                        break;
                }
            }
            //音量
            audioSource.volume = volume;
            //播放音源
            audioSource.Play();
            //獨奏處理
            if (soloSingleTrack)
            {
                MuteSoundsOnTrack(soundManagerTrack, true, 0f);
                audioSource.mute = false;
                if (autoUnSoloOnEnd)
                {
                    MuteSoundsOnTrack(soundManagerTrack, false, audioClip.length);
                }
            }
            else if (soloAllTracks)
            {
                MuteAllSounds(0f);
                audioSource.mute = false;
                if (autoUnSoloOnEnd)
                {
                    MuteAllSounds(audioClip.length, false);
                }
            }
            //準備儲存暫存音源
            sound.ID = ID;
            sound.Track = soundManagerTrack;
            sound.Source = audioSource;
            sound.Persistent = persistent;
            //檢查是否已經在暫存列表內
            bool alreadyIn = false;
            for (int i = 0; i < sounds.Count; i++)
            {
                if (sounds[i].Source == audioSource)
                {
                    sounds[i] = sound;
                    alreadyIn = true;
                }
            }

            if (!alreadyIn)
            {
                sounds.Add(sound);
            }

            return audioSource;
        }
        #endregion
        #region SoundControl
        public virtual void PauseSound(AudioSource source)
        {
            source.Pause();
        }
        public virtual void ResumeSound(AudioSource source)
        {
            source.Play();
        }
        public virtual void StopSound(AudioSource source)
        {
            source.Stop();
        }
        public virtual void DisableSound(AudioSource source)
        {
            source.Stop();
            soundPool.Despawn(source);
            sounds.RemoveAll(sound => sound.Source == source);
        }
        #endregion
        #region AllSoundsControls
        public virtual void PauseAllSounds()
        {
            foreach (SoundMGRSound sound in sounds)
            {
                sound.Source.Pause();
            }
        }
        public virtual void PlayAllSounds()
        {
            foreach (SoundMGRSound sound in sounds)
            {
                sound.Source.Play();
            }
        }
        public virtual void StopAllSounds()
        {
            foreach (SoundMGRSound sound in sounds)
            {
                sound.Source.Stop();
            }
        }
        public virtual void DisableAllSounds()
        {
            foreach (SoundMGRSound sound in sounds.Reverse<SoundMGRSound>())
            {
                if (sound.Source != null)
                {
                    DisableSound(sound.Source);
                }
            }
        }
        public virtual void DisableAllSoundsIgnorePersistent()
        {
            foreach (SoundMGRSound sound in sounds.Reverse<SoundMGRSound>())
            {
                if ((!sound.Persistent) && (sound.Source != null))
                {
                    DisableSound(sound.Source);
                }
            }
        }
        public virtual void DisableAllLoopingSounds()
        {
            foreach (SoundMGRSound sound in sounds.Reverse<SoundMGRSound>())
            {
                if ((sound.Source.loop) && (sound.Source != null))
                {
                    DisableSound(sound.Source);
                }
            }
        }
        #endregion
        #region TrackControls
        public virtual void MuteTrack(SoundManagerTracks track)
        {
            ControlTrack(track, ControlTrackModes.Mute, 0f);
        }

        public virtual void UnmuteTrack(SoundManagerTracks track)
        {
            ControlTrack(track, ControlTrackModes.Unmute, 0f);
        }

        public virtual void SetTrackVolume(SoundManagerTracks track, float volume)
        {
            ControlTrack(track, ControlTrackModes.SetVolume, volume);
        }

        public virtual float GetTrackVolume(SoundManagerTracks track, bool mutedVolume)
        {
            switch (track)
            {
                case SoundManagerTracks.Master:
                    if (mutedVolume)
                    {
                        return settingsSo.Settings.MutedMasterVolume;
                    }
                    else
                    {
                        return settingsSo.Settings.MasterVolume;
                    }
                case SoundManagerTracks.Music:
                    if (mutedVolume)
                    {
                        return settingsSo.Settings.MutedMusicVolume;
                    }
                    else
                    {
                        return settingsSo.Settings.MusicVolume;
                    }
                case SoundManagerTracks.Sfx:
                    if (mutedVolume)
                    {
                        return settingsSo.Settings.MutedSfxVolume;
                    }
                    else
                    {
                        return settingsSo.Settings.SfxVolume;
                    }
                case SoundManagerTracks.UI:
                    if (mutedVolume)
                    {
                        return settingsSo.Settings.MutedUIVolume;
                    }
                    else
                    {
                        return settingsSo.Settings.UIVolume;
                    }
            }

            return 1f;
        }

        public virtual void PauseTrack(SoundManagerTracks track)
        {
            foreach (SoundMGRSound sound in sounds)
            {
                if (sound.Track == track)
                {
                    sound.Source.Pause();
                }
            }
        }

        public virtual void PlayTrack(SoundManagerTracks track)
        {
            foreach (SoundMGRSound sound in sounds)
            {
                if (sound.Track == track)
                {
                    sound.Source.Play();
                }
            }
        }

        public virtual void StopTrack(SoundManagerTracks track)
        {
            foreach (SoundMGRSound sound in sounds)
            {
                if (sound.Track == track)
                {
                    sound.Source.Stop();
                }
            }
        }

        public virtual void DisableTrack(SoundManagerTracks track)
        {
            foreach (SoundMGRSound sound in sounds)
            {
                if (sound.Track == track)
                {
                    sound.Source.Stop();
                    soundPool.Despawn(sound.Source);
                }
            }
            sounds.RemoveAll(sound => sound.Track == track);
        }

        public virtual void MuteMusic() { MuteTrack(SoundManagerTracks.Music); }

        public virtual void UnmuteMusic() { UnmuteTrack(SoundManagerTracks.Music); }

        public virtual void MuteSfx() { MuteTrack(SoundManagerTracks.Sfx); }

        public virtual void UnmuteSfx() { UnmuteTrack(SoundManagerTracks.Sfx); }

        public virtual void MuteUI() { MuteTrack(SoundManagerTracks.UI); }

        public virtual void UnmuteUI() { UnmuteTrack(SoundManagerTracks.UI); }

        public virtual void MuteMaster() { MuteTrack(SoundManagerTracks.Master); }

        public virtual void UnmuteMaster() { UnmuteTrack(SoundManagerTracks.Master); }

        public virtual void SetVolumeMusic(float newVolume) { SetTrackVolume(SoundManagerTracks.Music, newVolume); }

        public virtual void SetVolumeSfx(float newVolume) { SetTrackVolume(SoundManagerTracks.Sfx, newVolume); }

        public virtual void SetVolumeUI(float newVolume) { SetTrackVolume(SoundManagerTracks.UI, newVolume); }

        public virtual void SetVolumeMaster(float newVolume) { SetTrackVolume(SoundManagerTracks.Master, newVolume); }

        public enum ControlTrackModes { Mute, Unmute, SetVolume }

        protected virtual void ControlTrack(SoundManagerTracks track, ControlTrackModes trackMode, float volume = 0.5f)
        {
            string target = "";
            float savedVolume = 0f;

            switch (track)
            {
                case SoundManagerTracks.Master:
                    target = settingsSo.Settings.MasterVolumeParameter;
                    if (trackMode == ControlTrackModes.Mute) { settingsSo.TargetAudioMixer.GetFloat(target, out settingsSo.Settings.MutedMasterVolume); settingsSo.Settings.MasterOn = false; }
                    else if (trackMode == ControlTrackModes.Unmute) { savedVolume = settingsSo.Settings.MutedMasterVolume; settingsSo.Settings.MasterOn = true; }
                    break;
                case SoundManagerTracks.Music:
                    target = settingsSo.Settings.MusicVolumeParameter;
                    if (trackMode == ControlTrackModes.Mute) { settingsSo.TargetAudioMixer.GetFloat(target, out settingsSo.Settings.MutedMusicVolume); settingsSo.Settings.MusicOn = false; }
                    else if (trackMode == ControlTrackModes.Unmute) { savedVolume = settingsSo.Settings.MutedMusicVolume; settingsSo.Settings.MusicOn = true; }
                    break;
                case SoundManagerTracks.Sfx:
                    target = settingsSo.Settings.SfxVolumeParameter;
                    if (trackMode == ControlTrackModes.Mute) { settingsSo.TargetAudioMixer.GetFloat(target, out settingsSo.Settings.MutedSfxVolume); settingsSo.Settings.SfxOn = false; }
                    else if (trackMode == ControlTrackModes.Unmute) { savedVolume = settingsSo.Settings.MutedSfxVolume; settingsSo.Settings.SfxOn = true; }
                    break;
                case SoundManagerTracks.UI:
                    target = settingsSo.Settings.UIVolumeParameter;
                    if (trackMode == ControlTrackModes.Mute) { settingsSo.TargetAudioMixer.GetFloat(target, out settingsSo.Settings.MutedUIVolume); settingsSo.Settings.UIOn = false; }
                    else if (trackMode == ControlTrackModes.Unmute) { savedVolume = settingsSo.Settings.MutedUIVolume; settingsSo.Settings.UIOn = true; }
                    break;
            }

            switch (trackMode)
            {
                case ControlTrackModes.Mute:
                    settingsSo.SetTrackVolume(track, 0f);
                    break;
                case ControlTrackModes.Unmute:
                    settingsSo.SetTrackVolume(track, settingsSo.MixerVolumeToNormalized(savedVolume));
                    break;
                case ControlTrackModes.SetVolume:
                    settingsSo.SetTrackVolume(track, volume);
                    break;
            }

            settingsSo.GetTrackVolumes();
        }

        #endregion
        #region Find
        public virtual AudioSource FindByID(int ID)
        {
            foreach (SoundMGRSound sound in sounds)
            {
                if (sound.ID == ID)
                {
                    return sound.Source;
                }
            }

            return null;
        }
        public virtual AudioSource FindByClip(AudioClip clip)
        {
            foreach (SoundMGRSound sound in sounds)
            {
                if (sound.Source.clip == clip)
                {
                    return sound.Source;
                }
            }

            return null;
        }
        #endregion
        #region Solo
        public virtual async void MuteSoundsOnTrack(SoundManagerTracks track, bool mute, float delay)
        {
            if (delay > 0)
            {
                await UniTask.Delay(Mathf.RoundToInt(delay * 1000), true);
            }

            foreach (SoundMGRSound sound in sounds)
            {
                if (sound.Track == track)
                {
                    sound.Source.mute = mute;
                }
            }
        }
        public virtual async void MuteAllSounds(float delay, bool mute = true)
        {
            if (delay > 0)
            {
                await UniTask.Delay(Mathf.RoundToInt(delay * 1000), true);
            }
            foreach (SoundMGRSound sound in sounds)
            {
                sound.Source.mute = mute;
            }
        }
        #endregion
        #endregion
        #region Protected Method
        protected override void OnAwake()
        {
            GameObject temporaryAudioHost = new GameObject("AudioSourceItem");
            AudioSource tempSource = temporaryAudioHost.AddComponent<AudioSource>();
            tempSource.playOnAwake = false;
            if (soundPool == null)
            {
                soundPool = new ObjectPool<AudioSource>(temporaryAudioHost, AudioSourcePoolSize, transform);
            }
            sounds = new List<SoundMGRSound>();
        }
        protected virtual void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
        protected virtual void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }
        protected override string GetSingletonName()
        {
            return "SoundManager";
        }
        protected override bool IsDestroyOnLoad()
        {
            return false;
        }
        #region Event
        protected virtual void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            DisableAllSoundsIgnorePersistent();
        }
        #endregion
        #endregion
        #region Private Method
        private async void AutoDisableAudioSource(float duration, AudioSource source, AudioClip clip)
        {
            var time = Mathf.RoundToInt(duration * 1000);
            await UniTask.Delay(time);
            if (source.clip != clip)
            {
                return;
            }
            DisableSound(source);
        }
        #endregion
    }
}
