﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
namespace Public.Tools
{
    [Serializable]
    public struct SoundMGRPlayOptions
    {
        #region AudioSource 選項
        /// <summary>
        /// 使用的AudioMixerGroup。
        /// </summary>
        public AudioMixerGroup AudioGroup;
        /// <summary>
        /// 打開/關閉所有效果。
        /// </summary>
        public bool BypassEffects;
        /// <summary>
        /// 快速打開/關閉所有監聽器效果。
        /// </summary>
        public bool BypassListenerEffects;
        /// <summary>
        /// 快速打開/關閉所有混響區域。
        /// </summary>
        public bool BypassReverbZones;
        /// <summary>
        /// 是否循環播放
        /// </summary>
        public bool Loop;
        /// <summary>
        /// 設置 AudioSource 的優先級。
        /// </summary>
        public int Priority;
        /// <summary>
        /// 播放音量
        /// </summary>
        public float Volume;
        /// <summary>
        /// 音頻音高。
        /// </summary>
        public float Pitch;
        #endregion
        #region Other
        /// <summary>
        /// 播放軌道
        /// </summary>
        public SoundMGR.SoundManagerTracks ESoundManagerTrack;
        /// <summary>
        /// 聲音ID，搜尋聲音使用。
        /// </summary>
        public int ID;
        /// <summary>
        /// 是否過場時持續存在。
        /// </summary>
        public bool Persistent;
        /// <summary>
        /// 是否在其目標軌道上以獨奏模式播放聲音。 如果為True，則聲音開始播放時，目標軌道上的所有其他聲音將被靜音。
        /// </summary>
        public bool SoloSingleTrack;
        /// <summary>
        /// 是否在所有軌道上以獨奏模式播放聲音。 如果為True，則聲音開始播放時，其他音軌都將靜音。
        /// </summary>
        public bool SoloAllTracks;
        /// <summary>
        /// 在任何獨奏模式下，AutoUnSoloOnEnd 將在聲音停止播放後，自動取消靜音。
        /// </summary>
        public bool AutoUnSoloOnEnd;
        #endregion
        public static SoundMGRPlayOptions Default
        {
            get
            {
                SoundMGRPlayOptions defaultOptions = new SoundMGRPlayOptions();
                defaultOptions.BypassEffects = false;
                defaultOptions.BypassListenerEffects = false;
                defaultOptions.BypassReverbZones = false;
                defaultOptions.Loop = false;
                defaultOptions.Priority = 128;
                defaultOptions.Volume = 1.0f;
                defaultOptions.Pitch = 1f;
                defaultOptions.ESoundManagerTrack = SoundMGR.SoundManagerTracks.Sfx;
                defaultOptions.ID = 0;
                defaultOptions.Persistent = false;
                defaultOptions.SoloSingleTrack = false;
                defaultOptions.SoloAllTracks = false;
                defaultOptions.AutoUnSoloOnEnd = false;
                return defaultOptions;
            }
        }
        public static SoundMGRPlayOptions LoopPersistent
        {
            get
            {
                SoundMGRPlayOptions defaultOptions = Default;
                defaultOptions.Loop = true;
                defaultOptions.Persistent = true;
                return defaultOptions;
            }
        }
    }
}
