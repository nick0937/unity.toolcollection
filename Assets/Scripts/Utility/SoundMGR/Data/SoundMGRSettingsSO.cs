﻿using System;
using UnityEngine;
using UnityEngine.Audio;
namespace Public.Tools
{
    [CreateAssetMenu(menuName = "Philippines/Audio/SoundManagerSettings")]
    public class SoundMGRSettingsSO : ScriptableObject
    {
        [Header("Audio Mixer")]
        [Tooltip("播放聲音時使用的AudioMixer")]
        public AudioMixer TargetAudioMixer;
        [Tooltip("MasterGroup")]
        public AudioMixerGroup MasterAudioMixerGroup;
        [Tooltip("MusicGroup")]
        public AudioMixerGroup MusicAudioMixerGroup;
        [Tooltip("SfxGroup")]
        public AudioMixerGroup SfxAudioMixerGroup;
        [Tooltip("UIGroup")]
        public AudioMixerGroup UIAudioMixerGroup;

        [Header("Settings Unfold")]
        [Tooltip("SoundManager設定值")]
        public SoundMGRSettings Settings;

        #region Volume
        public virtual void SetTrackVolume(SoundMGR.SoundManagerTracks track, float volume)
        {
            if (volume <= 0f)
            {
                volume = SoundMGRSettings.MinimalVolume;
            }

            switch (track)
            {
                case SoundMGR.SoundManagerTracks.Master:
                    TargetAudioMixer.SetFloat(Settings.MasterVolumeParameter, NormalizedToMixerVolume(volume));
                    Settings.MasterVolume = volume;
                    break;
                case SoundMGR.SoundManagerTracks.Music:
                    TargetAudioMixer.SetFloat(Settings.MusicVolumeParameter, NormalizedToMixerVolume(volume));
                    Settings.MusicVolume = volume;
                    break;
                case SoundMGR.SoundManagerTracks.Sfx:
                    TargetAudioMixer.SetFloat(Settings.SfxVolumeParameter, NormalizedToMixerVolume(volume));
                    Settings.SfxVolume = volume;
                    break;
                case SoundMGR.SoundManagerTracks.UI:
                    TargetAudioMixer.SetFloat(Settings.UIVolumeParameter, NormalizedToMixerVolume(volume));
                    Settings.UIVolume = volume;
                    break;
            }
        }
        public virtual float GetTrackVolume(SoundMGR.SoundManagerTracks track)
        {
            float volume = 1f;
            switch (track)
            {
                case SoundMGR.SoundManagerTracks.Master:
                    TargetAudioMixer.GetFloat(Settings.MasterVolumeParameter, out volume);
                    break;
                case SoundMGR.SoundManagerTracks.Music:
                    TargetAudioMixer.GetFloat(Settings.MusicVolumeParameter, out volume);
                    break;
                case SoundMGR.SoundManagerTracks.Sfx:
                    TargetAudioMixer.GetFloat(Settings.SfxVolumeParameter, out volume);
                    break;
                case SoundMGR.SoundManagerTracks.UI:
                    TargetAudioMixer.GetFloat(Settings.UIVolumeParameter, out volume);
                    break;
            }

            return MixerVolumeToNormalized(volume);
        }
        public virtual void GetTrackVolumes()
        {
            Settings.MasterVolume = GetTrackVolume(SoundMGR.SoundManagerTracks.Master);
            Settings.MusicVolume = GetTrackVolume(SoundMGR.SoundManagerTracks.Music);
            Settings.SfxVolume = GetTrackVolume(SoundMGR.SoundManagerTracks.Sfx);
            Settings.UIVolume = GetTrackVolume(SoundMGR.SoundManagerTracks.UI);
        }
        public virtual float NormalizedToMixerVolume(float normalizedVolume)
        {
            return Mathf.Log10(normalizedVolume) * 20;
        }
        public virtual float MixerVolumeToNormalized(float mixerVolume)
        {
            return (float)Math.Pow(10, (mixerVolume / 20));
        }

        #endregion Volume
    }
}
