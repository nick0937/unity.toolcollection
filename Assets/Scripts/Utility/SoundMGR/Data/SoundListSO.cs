﻿using Public.Tool;
using System;
using System.Collections.Generic;
using UnityEngine;
namespace Public.Tools
{
    [CreateAssetMenu(menuName = "Philippines/Audio/SoundList")]
    public class SoundListSO : ScriptableObject
    {
        [SerializeField]
        List<ClipInfo> soundList;
        public List<ClipInfo> SoundList => soundList;

        [SerializeField]
        SoundMGR.SoundManagerTracks track;
        public SoundMGR.SoundManagerTracks Track => track;

        public ClipInfo ReadData(string clipName)
        {
            foreach(var clip in SoundList)
            {
                if(clip.Name == clipName)
                {
                    return clip;
                }
            }
            return null;
        }
    }
    [Serializable]
    public class ClipInfo
    {
        public int ID;
        public string Name;
        public AudioClip Clip;
    }
}
