﻿using Public.Tool;
using System;
using UnityEngine;
namespace Public.Tools
{
    [Serializable]
    public class SoundMGRSettings
    {
        public const float MinimalVolume = 0.0001f;
        public const float MaxVolume = 10f;
        public const float DefaultVolume = 1f;

        [Header("Audio Mixer 參數")]
        [Tooltip("AudioMixer-MasterVolume 參數名稱")]
        public string MasterVolumeParameter = "MasterVolume";
        [Tooltip("AudioMixer-MusicVolume 參數名稱")]
        public string MusicVolumeParameter = "MusicVolume";
        [Tooltip("AudioMixer-SfxVolume 參數名稱")]
        public string SfxVolumeParameter = "SfxVolume";
        [Tooltip("AudioMixer-UIVolume 參數名稱")]
        public string UIVolumeParameter = "UIVolume";

        [Header("Master")]
        [Range(MinimalVolume, MaxVolume)]
        [Tooltip("Master音量")]
        public float MasterVolume = DefaultVolume;
        [Tooltip("是否開啟Master音軌")]
        [ReadOnly]
        public bool MasterOn = true;
        [Tooltip("Master音軌靜音前的音量")]
        [ReadOnly]
        public float MutedMasterVolume;

        [Header("Music")]
        [Range(MinimalVolume, MaxVolume)]
        [Tooltip("Music音量")]
        public float MusicVolume = DefaultVolume;
        [Tooltip("是否開啟Music音軌")]
        [ReadOnly]
        public bool MusicOn = true;
        [Tooltip("Music音軌靜音前的音量")]
        [ReadOnly]
        public float MutedMusicVolume;

        [Header("Sfx")]
        [Range(MinimalVolume, MaxVolume)]
        [Tooltip("Sfx音量")]
        public float SfxVolume = DefaultVolume;
        [Tooltip("是否開啟Sfx音軌")]
        [ReadOnly]
        public bool SfxOn = true;
        [Tooltip("Sfx音軌靜音前的音量")]
        [ReadOnly]
        public float MutedSfxVolume;

        [Header("UI")]
        [Range(MinimalVolume, MaxVolume)]
        [Tooltip("UI音量")]
        public float UIVolume = DefaultVolume;
        [Tooltip("是否開啟UI音軌")]
        [ReadOnly]
        public bool UIOn = true;
        [Tooltip("UI音軌靜音前的音量")]
        [ReadOnly]
        public float MutedUIVolume;
    }
}
