﻿using System;
using UnityEngine;

namespace Public.Tools
{
    [Serializable]
    public struct SoundMGRSound
    {
        /// <summary>
        /// 聲音ID，搜尋聲音使用。
        /// </summary>
        public int ID;
        /// <summary>
        /// 播放軌道
        /// </summary>
        public SoundMGR.SoundManagerTracks Track;
        /// <summary>
        /// 聲音源
        /// </summary>
        public AudioSource Source;
        /// <summary>
        /// 是否過場時持續存在。
        /// </summary>
        public bool Persistent;
    }
}
