﻿using System.Collections;
using UnityEngine;


public sealed class CoroutineWaitForSeconds : IEnumerator
{
    float beginTime;
    float waitSecondsTime;
    object IEnumerator.Current { get { return null; } }

    public CoroutineWaitForSeconds()
    {
    }

    public CoroutineWaitForSeconds Wait(float waitSecondTime)
    {
        beginTime = Time.time;
        waitSecondsTime = waitSecondTime;

        return this;
    }

    bool IEnumerator.MoveNext()
    {
        return (Time.time - beginTime) <= waitSecondsTime;
    }

    void IEnumerator.Reset()
    {
    }
}


