﻿using NickABC.Utils.Other;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility.Extensions;

namespace NickABC.Utils
{
    public class CoroutineMGR : CustomYieldInstruction
    {
        public static Pool<CoroutineMGR, MonoBehaviour> CoroutineMGR_Pool = new Pool<CoroutineMGR, MonoBehaviour>((c, m) => c.Setup(m), c => c.Clear());
        public Pool<Coroutines> CoroutinePool = new Pool<Coroutines>(null, c => c.Clear());

        public bool IsRunning { get; private set; }

        private MonoBehaviour m_player;
        private Queue<Coroutines> m_Queue = new Queue<Coroutines>();
        private Coroutines m_Coroutines;

        protected private Action m_CompleteAct = null;
        protected private bool m_Completed = false;

        public override bool keepWaiting
        {
            get
            {
                return IsRunning;
            }
        }

        public CoroutineMGR RunRoutine(IEnumerator i_enumerator)
        {
            m_Queue.Enqueue(CoroutinePool.Spawn().AddRoutine(i_enumerator, m_player));
            return this;
        }

        public CoroutineMGR Invoke(Action i_action)
        {
            m_Queue.Enqueue(CoroutinePool.Spawn().AddAction(i_action));
            return this;
        }

        public CoroutineMGR DelayInvoke(Action i_callback, float i_delay)
        {
            m_Queue.Enqueue(CoroutinePool.Spawn().AddRoutine(ExtensionMethods.WaitForSeconds(i_callback, i_delay), m_player));
            return this;
        }

        public CoroutineMGR DelayFrameInvoke(Action i_callback, int i_frames = 1)
        {
            m_Queue.Enqueue(CoroutinePool.Spawn().AddRoutine(ExtensionMethods.WaitForEndOfFrame(i_callback, i_frames), m_player));
            return this;
        }

        public CoroutineMGR DelayFlagInvoke(Action i_callback, Func<bool> i_flag)
        {
            m_Queue.Enqueue(CoroutinePool.Spawn().AddRoutine(ExtensionMethods.WaitForFlag(i_callback, i_flag), m_player));
            return this;
        }

        public CoroutineMGR LoopInvoke(Action i_callback, Func<bool> i_stop)
        {
            m_Queue.Enqueue(CoroutinePool.Spawn().AddRoutine(ExtensionMethods.LoopAction(i_callback, i_stop), m_player));
            return this;
        }

        public CoroutineMGR LoopBySecInvoke(Action i_callback, float i_delay, Func<bool> i_stop)
        {
            m_Queue.Enqueue(CoroutinePool.Spawn().AddRoutine(ExtensionMethods.IntervalLoopAction(i_callback, i_delay, i_stop), m_player));
            return this;
        }

        public virtual CoroutineMGR OnComplete(Action i_Complete)
        {
            m_CompleteAct = i_Complete;
            return this;
        }

        protected internal virtual void Complete()
        {
            if (m_Completed) return;

            m_Completed = true;
            Clear();
            if (m_Coroutines.IsNull() == false)
            {
                m_Coroutines.StopCoroutine();
            }
            if (m_CompleteAct.IsNull() == false)
            {
                try
                {
                    m_CompleteAct.Invoke();
                }
                catch (System.Exception e)
                {
                    Debug.LogError("m_CompleteAct.Invoke() Exception:" + e);
                }
            }
            m_CompleteAct = null;
        }

        private CoroutineMGR Setup(MonoBehaviour i_player)
        {
            IsRunning = true;
            m_Completed = false;
            m_player = i_player;
            m_player.StartCoroutine(Routine());
            return this;
        }
        private IEnumerator Routine()
        {
            yield return null;
            while (m_Queue.Count > 0)
            {
                m_Coroutines = m_Queue.Dequeue();
                var r = m_Coroutines.Go();
                if (r.IsNull() == false)
                    yield return r;

                CoroutinePool.Despawn(m_Coroutines);
                m_Coroutines = null;
            }

            IsRunning = false;
            Complete();
            CoroutineMGR_Pool.Despawn(this);
        }
        private void Clear()
        {
            m_player = null;
            m_Queue.Clear();
        }
    }

    public class Coroutines
    {
        private eType m_type;
        private MonoBehaviour m_player;
        private IEnumerator m_routine;
        private Action m_action;

        public Coroutine Go()
        {
            switch (m_type)
            {
                default:
                case eType.NonCoroutine:
                    m_action();
                    return null;
                case eType.Coroutine:
                    return m_player.StartCoroutine(m_routine);
            }
        }
        public enum eType
        {
            NonCoroutine,
            Coroutine
        }
        public Coroutines AddRoutine(IEnumerator i_routine, MonoBehaviour i_player)
        {
            m_type = eType.Coroutine;
            this.m_player = i_player;
            this.m_routine = i_routine;
            return this;
        }
        public Coroutines AddAction(Action i_action)
        {
            m_type = eType.NonCoroutine;
            this.m_action = i_action;
            return this;
        }
        public void StopCoroutine()
        {
            if (m_routine.IsNull("babababab") == false)
                m_player.StopCoroutine(m_routine);
        }
        public void Clear()
        {
            m_player = null;
            m_routine = null;
            m_action = null;
        }
    }
}
