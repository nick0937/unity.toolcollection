/*
 *  Project Name：philippines
 *
 *  Programmer：Nick　Tseng
 *
 *  Create Date：2022/10/04
 *  
 *  UnityVersion：2020.3.15f2
 */

using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine.ResourceManagement.AsyncOperations;
using Utility.Tool;

namespace UnityEngine.AddressableAssets
{
    public static partial class AddressablesManager
    {
        public static void GetDownloadSizeAsync(string key
                                        , Action<long> onSucceeded
                                        , Action onFailed = null)
        {
            GetDownloadSizeAsync(new List<string>() { key }, onSucceeded, onFailed);
        }
        public static void GetDownloadSizeAsync<T>(IEnumerable<T> keys
                                        , Action<long> onSucceeded
                                        , Action onFailed = null)
        {
            try
            {
                var operation = Addressables.GetDownloadSizeAsync(keys);
                operation.Completed += (handle) =>
                {
                    OnGetDownloadSizeCompleted(handle, onSucceeded, onFailed);
                    Addressables.Release(operation);
                };
            }
            catch (Exception ex)
            {
                if (ExceptionHandle == ExceptionHandleType.Throw)
                    throw ex;

                if (ExceptionHandle == ExceptionHandleType.Log)
                    Debug.LogException(ex);

                onFailed?.Invoke();
            }
        }
        public static void DownloadAddressableAsset(string key
                                        , Action onSucceeded
                                        , Action onFailed = null
                                        , Action<float, string> onProgress = null
                                        , CancellationToken cancelToken = default)
        {
            DownloadAddressableAsset(new List<string>() { key }, onSucceeded, onFailed, onProgress, cancelToken);
        }
        public static async void DownloadAddressableAsset<T>(IEnumerable<T> keys
                                        , Action onSucceeded
                                        , Action onFailed = null
                                        , Action<float, string> onProgress = null
                                        , CancellationToken cancelToken = default)
        {
            try
            {
                var operation = Addressables.DownloadDependenciesAsync(keys, Addressables.MergeMode.Union);
                await UniTask.WaitWhile(() =>
                {
                    onProgress?.Invoke(operation.GetDownloadStatus().Percent, $"{SizeConverter.Inst.FormatSize(operation.GetDownloadStatus().DownloadedBytes)}/{SizeConverter.Inst.FormatSize(operation.GetDownloadStatus().TotalBytes)}");
                    return !operation.GetDownloadStatus().IsDone;
                }, cancellationToken: cancelToken);

                if (cancelToken.IsCancellationRequested) return;

                operation.Completed += (handle) =>
                {
                    OnDownAddressableAssetCompleted(handle, onSucceeded, onFailed);
                    Addressables.Release(operation);
                };
            }
            catch (Exception ex)
            {
                if (ExceptionHandle == ExceptionHandleType.Throw)
                    throw ex;

                if (ExceptionHandle == ExceptionHandleType.Log)
                    Debug.LogException(ex);

                onFailed?.Invoke();
            }
        }

        private static void OnGetDownloadSizeCompleted<T>(AsyncOperationHandle<T> handle,
                                                    Action<T> onSucceeded = null,
                                                    Action onFailed = null)
        {
            if (handle.Status != AsyncOperationStatus.Succeeded)
            {
                onFailed?.Invoke();
                return;
            }

            onSucceeded?.Invoke(handle.Result);
        }
        private static void OnDownAddressableAssetCompleted(AsyncOperationHandle handle,
                                                    Action onSucceeded = null,
                                                    Action onFailed = null)
        {
            if (handle.Status != AsyncOperationStatus.Succeeded)
            {
                onFailed?.Invoke();
                return;
            }

            onSucceeded?.Invoke();
        }
    }
}