/*
 *  檔案 : ServiceLocatorBase.cs
 *  
 *  說明 : 服務定位器
 *      
 *      *** 建議於場景初始化(Awake())時註冊 ***
 *      
 *      ** 服務類別需有 IGameService 介面 **
 *      ** 依需求填入服務的初始化參數 InitialzeArgs, 不需要則空白即可 **
 *      ** 其餘介面(IInitializable, IUpdatable)可選擇使用, IInitializable -> Initialize() 會在各服務類別的 Awake() 之前執行 **
 *      ** 如需跨場景使用，請override IsDestroyOnLoad >>return false<< **
 *      
 *      1. Register 註冊新服務: 
 *      
 *          新增服務類別
 *      
 *      2. RegisterMono 註冊新服務: 
 *          
 *          新增帶有Mono服務類別，並實例化為子物件。
 *      
 *      3. Unregister 註銷服務: 
 *      
 *          指定註銷服務列表內的服務類別。
 *          ** ，如該 ServiceLocator 為跨場景使用 (DontDestroyOnLoad = ture)，要註銷用不到的服務。 **
 *      
 *      4.GetService 取得服務: 
 *          
 *          取得服務列表中的指定服務
 */
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utility.Extensions;
using Utility.Singleton;

public interface IGameService { }
/// <summary>
/// 初始化相關參數
/// </summary>
public abstract class InitializeArgs { }
/// <summary>
/// 初始化介面
/// </summary>
public interface IInitializable
{
    /// <summary>
    /// 初始化狀態
    /// </summary>
    void Initialize(InitializeArgs args);
}
/// <summary>
/// 更新狀態介面
/// </summary>
public interface IUpdatable
{
    /// <summary>
    /// 更新狀態
    /// </summary>
    void OnUpdate();
}
public interface IDestroyable
{
    /// <summary>
    /// 刪除狀態
    /// </summary>
    void OnDestroyEvent();
}
namespace Utility.ServiceLocator
{
    public abstract class ServiceLocatorBase<T> : MonoSingleton<T> where T : MonoBehaviour
    {
        private Dictionary<Type, IGameService> _serviceDictionary = new Dictionary<Type, IGameService>();
        private Dictionary<Type, MonoBehaviour> _serviceMonoDictionary = new Dictionary<Type, MonoBehaviour>();
        private List<IUpdatable> _updatableList = new List<IUpdatable>();
        /// <summary>
        /// 註冊新服務
        /// </summary>
        public virtual void Register<U>(InitializeArgs initialzeParams = null, bool autoInitialize = true) where U : IGameService, new()
        {
            Type serviceType = typeof(U);
            UnityEngine.Assertions.Assert.IsFalse(_serviceDictionary.ContainsKey(serviceType), $"[{name}] The <{serviceType.Name}> service already exists");
            UnityEngine.Assertions.Assert.IsFalse(serviceType.IsSubclassOf(typeof(MonoBehaviour)), $"[{name}] This Service <{serviceType.Name}> is a MonoBehaviour, use 'RegisterMono ()' instead");

            Register(new U(), initialzeParams, autoInitialize);
        }
        /// <summary>
        /// 註冊新服務-MonoBehaviour
        /// </summary>
        public virtual void RegisterMono<U>(InitializeArgs initialzeParams = null, bool autoInitialize = true, Transform parentObject = null, bool joinParentObject = false) where U : MonoBehaviour, IGameService
        {
            Type serviceType = typeof(U);
            UnityEngine.Assertions.Assert.IsFalse(_serviceDictionary.ContainsKey(serviceType), $"[{name}] The <{serviceType.Name}> service already exists");

            U service;
            U[] managers = FindObjectsOfType(typeof(U)) as U[];

            if (parentObject.IsNull())
            {
                parentObject = transform;
            }

            if (managers.Length != 0)
            {
                if (managers.Length == 1)
                {//如已存在Hierarchy，更名後回傳。
                    service = managers[0].GetComponent<U>();
                    if (joinParentObject)
                    {
                        managers[0].transform.SetParent(parentObject);    //Sets the gameobject's parent to the ServiceLocator
                    }
                    Register(service, initialzeParams, autoInitialize);
                    _serviceMonoDictionary.Add(serviceType, service);
                    return;
                }
                else
                {//如Hierarchy大於兩個，Del All。
                    Debug.LogError($"You have more than one {typeof(T).Name} in the scene.");
                    foreach (U manager in managers)
                    {
                        Destroy(manager.gameObject);
                    }
                }
            }
            else
            {
                GameObject singletonObj;
                singletonObj = new GameObject();    //Create a gameobject
                singletonObj.name = $"{serviceType.Name}";   //Change gameobject name
                if (joinParentObject)
                {
                    singletonObj.transform.SetParent(parentObject);    //Sets the gameobject's parent to the ServiceLocator
                }
                service = singletonObj.AddComponent<U>();    //Add service component
                Register(service, initialzeParams, autoInitialize);
                _serviceMonoDictionary.Add(serviceType, service);
            }
        }
        /// <summary>
        /// 註銷服務
        /// </summary>
        public virtual void Unregister<U>() where U : IGameService
        {
            Type serviceType = typeof(U);
            UnityEngine.Assertions.Assert.IsTrue(_serviceDictionary.ContainsKey(serviceType), $"[{name}] Try to unregister a service of type {serviceType}, but the service is not registered with {GetType().Name}.");

            if (serviceType.IsSubclassOf(typeof(MonoBehaviour)))
            {
                Destroy(_serviceMonoDictionary[serviceType].gameObject);//刪除服務物件
                _serviceMonoDictionary.Remove(serviceType);
            }
            if (_serviceDictionary.TryGetValue(serviceType, out IGameService service))
            {
                if (service is IDestroyable destroyable)
                {
                    destroyable.OnDestroyEvent();
                }
            }
            _serviceDictionary.Remove(serviceType);
        }
        /// <summary>
        /// 註銷全部服務
        /// </summary>
        public virtual void UnregisterAll()
        {
            UnityEngine.Assertions.Assert.IsTrue(_serviceDictionary.Count > 0, $"[{name}] Try to unregister all service, but no service registered with {GetType().Name}.");
            var types = _serviceDictionary.Keys.ToArray();
            foreach (var type in types)
            {
                if (type.IsSubclassOf(typeof(MonoBehaviour)))
                {
                    Destroy(_serviceMonoDictionary[type].gameObject);//刪除服務物件
                    _serviceMonoDictionary.Remove(type);
                }
                if (_serviceDictionary.TryGetValue(type, out IGameService service))
                {
                    if (service is IDestroyable destroyable)
                    {
                        destroyable.OnDestroyEvent();
                    }
                }
                _serviceDictionary.Remove(type);
            }
        }
        /// <summary>
        /// 取得服務
        /// </summary>
        public virtual U GetService<U>() where U : class, IGameService
        {
            Type serviceType = typeof(U);
            if (_serviceDictionary.ContainsKey(serviceType))
            {
                return _serviceDictionary[serviceType] as U;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 取得服務列表
        /// </summary>
        public Dictionary<Type, IGameService> GetServiceDic()
        {
            return _serviceDictionary;
        }
        void Update()
        {
            OnUpdate();
        }
        /// <summary>
        /// 更新狀態
        /// </summary>
        protected virtual void OnUpdate()
        {
            foreach (var update in _updatableList)
            {
                update.OnUpdate();
            }
        }
        /// <summary>
        /// 註冊新服務
        /// </summary>
        protected virtual void Register<U>(U service, InitializeArgs initializeParams = null, bool autoInitialize = true) where U : IGameService
        {
            Type serviceType = typeof(U);
            UnityEngine.Assertions.Assert.IsFalse(_serviceDictionary.ContainsKey(serviceType), $"[{name}] The <{serviceType.Name}> service already exists");
            _serviceDictionary.Add(serviceType, service);

            if (service is IInitializable initializable && autoInitialize)   //If the registered Service has Initializable, the Initialize method is automatically called
            {
                initializable.Initialize(initializeParams);
            }

            if (service is IUpdatable updatable)   //If the registered Service has IUpdatable, Add to updatableList
            {
                _updatableList.Add(updatable);
            }
        }

    }
}