using System.Collections.Generic;
using UnityEngine;
namespace Utility.Other
{
    public static class DontDestroyOnLoadManager
    {
        private static List<GameObject> list_DDOLObject = new List<GameObject>();

        public static void DontDestroyOnLoad(this GameObject obj)
        {
            Object.DontDestroyOnLoad(obj);
            list_DDOLObject.Add(obj);
        }

        public static void DestroyAll()
        {
            foreach (var obj in list_DDOLObject)
            {
                if (obj != null)
                {
                    Object.Destroy(obj);
                }
            }
            list_DDOLObject.Clear();
        }
    }
}