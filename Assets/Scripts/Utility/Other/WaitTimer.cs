﻿using UnityEngine;
namespace NickABC.Utils
{
    /// <summary>
    /// 計時器
    /// </summary>
    public sealed class WaitTimer
    {
        /// <summary>
        /// 目前時間
        /// </summary>
        public float CurrentTime { get { return UpdateCurrentTime(); } }
        /// <summary>
        /// 時間長度
        /// </summary>
        public float Duration { get; private set; }
        /// <summary>
        /// 是否時間停止
        /// </summary>
        public bool IsStop { get; private set; }
        /// <summary>
        /// 是否時間到
        /// </summary>
        public bool IsTimeUp { get { return CurrentTime <= 0; } }
        /// <summary>
        /// 最後更新時間
        /// </summary>
        private float lastTime;
        /// <summary>
        /// 最後更新時間(每幀)
        /// </summary>
        private int lastUpdateFrame;
        /// <summary>
        /// 目前剩餘時間
        /// </summary>
        private float currentTime;
        /// <summary>
        /// 計時器
        /// </summary>
        /// <param name="duration">起始時間長度</param>
        /// <param name="autoStart">是否自動開始</param>
        public WaitTimer(float duration, bool autoStart = true)
        {
            IsStop = true;
            Duration = Mathf.Max(0f, duration);
            Reset(duration, !autoStart);
        }
        #region Public Method
        /// <summary>
        /// 計時開始
        /// </summary>
        public void Start()
        {
            Reset(Duration, false);
        }
        /// <summary>
        /// 重置計時器
        /// </summary>
        /// <param name="duration">時間長度</param>
        /// <param name="isStoped">是否暫停</param>
        public void Reset(float duration, bool isStoped = false)
        {
            UpdateLastTimeInfo();
            Duration = Mathf.Max(0f, duration);
            currentTime = Duration;
            IsStop = isStoped;
        }
        /// <summary>
        /// 計時器暂停
        /// </summary>
        public void Pause()
        {
            UpdateCurrentTime();
            IsStop = true;
        }
        /// <summary>
        /// 計時器繼續
        /// </summary>
        public void Continue()
        {
            UpdateLastTimeInfo();
            IsStop = false;
        }
        /// <summary>
        /// 計時器停止
        /// </summary>
        public void End()
        {
            currentTime = 0f;
            IsStop = true;
        }
        /// <summary>
        /// 取得整體倒數完成率
        /// </summary>
        /// <returns></returns>
        public float GetPercent()
        {
            UpdateCurrentTime();
            if (currentTime <= 0 || Duration <= 0)
                return 1f;

            return 1f - currentTime / Duration;
        }
        /// <summary>
        /// 取得單秒數倒數完成率
        /// </summary>
        /// <returns></returns>
        public float GetSecPercent()
        {
            UpdateCurrentTime();
            if (currentTime <= 0 || Duration <= 0)
                return 0f;

            return Mathf.Ceil(currentTime) - currentTime;
        }
        #endregion

        #region Private Method
        /// <summary>
        /// 計時器時間更新
        /// </summary>
        /// <returns>剩餘時間</returns>
        private float UpdateCurrentTime()
        {
            if (IsStop || lastUpdateFrame == Time.frameCount)
                return currentTime;

            currentTime -= Time.time - lastTime;
            UpdateLastTimeInfo();

            return currentTime;
        }
        /// <summary>
        /// 更新時間訊息
        /// </summary>
        private void UpdateLastTimeInfo()
        {
            lastTime = Time.time;
            lastUpdateFrame = Time.frameCount;
        }
        #endregion
    }
}
