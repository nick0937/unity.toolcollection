using System.Diagnostics;
using UnityEngine;

public static class Debug
{
    public const string conditionStr = "ENABLE_LOG";

    #region Log
    [Conditional(conditionStr)]
    public static void Log(object message)
    {
        UnityEngine.Debug.Log(message);
    }
    [Conditional(conditionStr)]
    public static void Log(object message, Object context)
    {
        UnityEngine.Debug.Log(message, context);
    }
    [Conditional(conditionStr)]
    public static void LogFormat(string message, params object[] args)
    {
        UnityEngine.Debug.LogFormat(message, args);
    }
    [Conditional(conditionStr)]
    public static void LogFormat(Object context, string message, params object[] args)
    {
        UnityEngine.Debug.LogFormat(context, message, args);
    }
    #endregion
    #region LogWarning
    [Conditional(conditionStr)]
    public static void LogWarning(object message)
    {
        UnityEngine.Debug.LogWarning(message);
    }
    [Conditional(conditionStr)]
    public static void LogWarning(object message, Object context)
    {
        UnityEngine.Debug.LogWarning(message, context);
    }
    [Conditional(conditionStr)]
    public static void LogWarningFormat(string message, params object[] args)
    {
        UnityEngine.Debug.LogWarningFormat(message, args);
    }
    [Conditional(conditionStr)]
    public static void LogWarningFormat(Object context, string message, params object[] args)
    {
        UnityEngine.Debug.LogWarningFormat(context, message, args);
    }
    #endregion
    #region LogError
    [Conditional(conditionStr)]
    public static void LogError(object message)
    {
        UnityEngine.Debug.LogError(message);
    }
    [Conditional(conditionStr)]
    public static void LogError(object message, Object context)
    {
        UnityEngine.Debug.LogError(message, context);
    }
    [Conditional(conditionStr)]
    public static void LogErrorFormat(string message, params object[] args)
    {
        UnityEngine.Debug.LogErrorFormat(message, args);
    }
    [Conditional(conditionStr)]
    public static void LogErrorFormat(Object context, string message, params object[] args)
    {
        UnityEngine.Debug.LogErrorFormat(context, message, args);
    }
    #endregion
    #region LogException
    [Conditional(conditionStr)]
    public static void LogException(System.Exception exception)
    {
        UnityEngine.Debug.LogException(exception);
    }
    [Conditional(conditionStr)]
    public static void LogException(System.Exception exception, Object context)
    {
        UnityEngine.Debug.LogException(exception, context);
    }
    #endregion
}