using UnityEngine;
namespace Utility.Other
{
    public static class MathsHelper
    {
        /// <summary>
        /// 指定次數的A到B數值變動，並返回像對應數值。
        /// </summary>
        /// <param name="start">起始點</param>
        /// <param name="target">目標點</param>
        /// <param name="amount">次數</param>
        public static float Approach(float start, float target, float amount)
        {
            if (start < target)
            {
                start += amount;
                if (start > target)
                {
                    return target;
                }
            }
            else
            {
                start -= amount;
                if (start < target)
                {
                    return target;
                }
            }
            return start;
        }

        public static float Remap(float x, float A, float B, float C, float D)
        {
            float remappedValue = C + (x - A) / (B - A) * (D - C);
            return remappedValue;
        }

        public static float RoundToDecimal(float value, int numberOfDecimals)
        {
            if (numberOfDecimals <= 0)
            {
                return Mathf.Round(value);
            }
            else
            {
                return Mathf.Round(value * 10f * numberOfDecimals) / (10f * numberOfDecimals);
            }
        }
    }
}
