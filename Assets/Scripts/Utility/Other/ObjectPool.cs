using UnityEngine;
using System;
using System.Collections.Generic;
using Utility.Extensions;

namespace Utility.Other
{
    public class ObjectPool<T> where T : Component
    {
        public class PoolObjectData
        {
            public T typeObj;
            public GameObject gameObject;
            public Transform transformObj;
        }

        private GameObject prefabReference;
        private List<PoolObjectData> pool;
        private List<PoolObjectData> inUsePool;
        private Transform poolContainer;
        private string poolName = "";
        private int totalPoolSize = 0;
        private Dictionary<string, PoolObjectData> objDicLookup;
        private Action<T> onCreateItem;
        private Action<T> onDespawnItem;

        public ObjectPool(GameObject prefab, int startSize, bool isActive = false, Transform parent = null, string poolNameOverride = "", Action<T> OnCreateItem = null, Action<T> OnDespawnItem = null)
        {
            prefabReference = prefab;

            onCreateItem = OnCreateItem;
            onDespawnItem = OnDespawnItem;

            pool = new List<PoolObjectData>();
            inUsePool = new List<PoolObjectData>();

            objDicLookup = new Dictionary<string, PoolObjectData>();

            poolName = poolNameOverride != "" ? poolNameOverride : typeof(T).Name;

            poolContainer = parent == null ? (new GameObject($"ObjectPool - {poolName}")).transform : parent;

            for (int idx = 0; idx < startSize; idx++)
            {
                AddPoolItem(isActive);
            }
        }

        PoolObjectData AddPoolItem(bool isActive = false)
        {
            GameObject newObj;

            newObj = UnityEngine.Object.Instantiate(prefabReference, poolContainer);

            Transform objTransform = newObj.transform;
            //objTransform.SetParent(poolContainer);

            /*if (newObj.TryGetComponent(out RectTransform rect))
            {
                rect.anchoredPosition3D = prefabReference.GetComponent<RectTransform>().anchoredPosition3D;
                rect.localScale = prefabReference.GetComponent<RectTransform>().localScale;
            }*/

            T objRef = newObj.GetOrAddComponent<T>();

            PoolObjectData object_data = new PoolObjectData() { gameObject = newObj, transformObj = objTransform, typeObj = objRef };

            objDicLookup.Add(objRef.GetHashCode().ToString(), object_data);

            pool.Add(object_data);

            newObj.SetActive(isActive);
            newObj.name = $"{poolName} #{totalPoolSize}";

            totalPoolSize++;

            if (onCreateItem != null)
                onCreateItem(objRef);

            return object_data;
        }

        public T Spawn(bool isActive = true)
        {
            T obj = null;

            if (pool.Count > 0)
            {
                PoolObjectData objectData = pool[0];
                objectData.gameObject.SetActive(isActive);

                obj = objectData.typeObj;

                pool.RemoveAt(0);
                inUsePool.Add(objectData);
            }
            else
            {
                PoolObjectData objectData = AddPoolItem(isActive);

                pool.RemoveAt(0);
                inUsePool.Add(objectData);

                objectData.gameObject.SetActive(isActive);

                obj = objectData.typeObj;
            }

            return obj;
        }

        public void Despawn(T obj)
        {
            int hash_code = obj.GetHashCode();

            if (objDicLookup.ContainsKey(hash_code.ToString()))
            {
                PoolObjectData object_data = objDicLookup[hash_code.ToString()];

                if (inUsePool.Contains(object_data))
                {
                    object_data.transformObj.SetParent(poolContainer);

                    if (onDespawnItem != null)
                        onDespawnItem(object_data.typeObj);
                    else
                        object_data.gameObject.SetActive(false);

                    inUsePool.Remove(object_data);
                    pool.Add(object_data);
                }
            }
            else
            {
                Debug.LogWarning("You're trying to recycle a pool object, which isn't already part of this pool");
            }
        }

        public void ResetPoolAll(Action<T> bespokeCallback = null)
        {
            foreach (PoolObjectData object_data in inUsePool)
            {
                object_data.gameObject.SetActive(false);
                object_data.transformObj.SetParent(poolContainer);

                pool.Add(object_data);

                if (bespokeCallback != null)
                    bespokeCallback(object_data.typeObj);
            }

            inUsePool = new List<PoolObjectData>();
        }

        public List<PoolObjectData> GetInUsePoolList()
        {
            return inUsePool;
        }
    }
}