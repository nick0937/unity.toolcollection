﻿using System;
using System.Collections.Generic;
namespace NickABC.Utils.Other
{
    public class Pool<T> where T : new()
    {

        public int Count
        {
            get
            {
                return pool.Count;
            }
        }

        private Stack<T> pool = new Stack<T>();

        private readonly Action<T> onSpawn;
        private readonly Action<T> onDespawn;

        public Pool(Action<T> onSpawn = null, Action<T> onDespawn = null)
        {
            this.onDespawn = onDespawn;
            this.onSpawn = onSpawn;
        }

        public T Spawn()
        {
            T item;
            if (pool.Count == 0)
            {
                item = new T();
            }
            else
            {
                item = pool.Pop();
            }
            onSpawn?.Invoke(item);

            return item;
        }

        public void Despawn(T item)
        {
            pool.Push(item);
            onDespawn?.Invoke(item);
        }
    }

    public class Pool<T, TParam> where T : new()
    {
        public int Count
        {
            get
            {
                return pool.Count;
            }
        }

        private Stack<T> pool = new Stack<T>();

        private readonly Action<T, TParam> onSpawn;
        private readonly Action<T> onDespawn;

        public Pool(Action<T, TParam> onSpawn = null, Action<T> onDespawn = null)
        {
            this.onDespawn = onDespawn;
            this.onSpawn = onSpawn;
        }

        public T Spawn(TParam init)
        {
            T item;
            if (pool.Count == 0)
            {
                item = new T();
            }
            else
            {
                item = pool.Pop();
            }
            onSpawn?.Invoke(item, init);

            return item;
        }

        public void Despawn(T item)
        {
            pool.Push(item);
            onDespawn?.Invoke(item);
        }
    }
}

