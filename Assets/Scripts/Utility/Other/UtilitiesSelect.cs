using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utility.Other
{
    public static partial class Utilities
    {
        public static GameObject Select(Hashtable findedCache, Transform rootTransform, string gameObjectNameOrPath)
        {
            GameObject[] obj = SelectMulti(findedCache, rootTransform, new string[1] { gameObjectNameOrPath });
            return obj == null ? null : obj.Length >= 1 ? obj[0] : null;
        }
        public static GameObject[] SelectMulti(Hashtable findedCache, Transform rootTransform, string[] gameObjectNameOrPathArray)
        {
            if (rootTransform == null)
            {
                Debug.LogError("rootTransform is NULL!!!!!");
                return null;
            }

            List<GameObject> gameObjectList = new List<GameObject>();
            List<Transform> allChildren = new List<Transform>();

            for (int i = 0; i < gameObjectNameOrPathArray.Length; i++)
            {
                if (findedCache[$"{rootTransform.gameObject.GetHashCode()}{gameObjectNameOrPathArray[i]}"] != null)
                {
                    gameObjectList.Add((GameObject)findedCache[$"{rootTransform.gameObject.GetHashCode()}{gameObjectNameOrPathArray[i]}"]);
                    continue;
                }

                if (gameObjectNameOrPathArray[i].StartsWith("/"))
                {//Path  ex. "/btnGroup"
                    Transform _transform = rootTransform.Find(gameObjectNameOrPathArray[i].Remove(0, 1));
                    if (_transform == null)
                    {
                        gameObjectList.Add(null);
                    }
                    else
                    {
                        gameObjectList.Add(_transform.gameObject);
                    }
                }
                else if (gameObjectNameOrPathArray[i].Contains("/"))
                {// Path  ex. "btnGroup/btn1"
                 //會以btnGroup為root，用"btnGroup/btn1"往下找
                    DepthFirstSearchAllChildren(rootTransform, ref allChildren);
                    string newRootName = gameObjectNameOrPathArray[i].Split('/')[0];
                    Transform root = null;
                    foreach (var child in allChildren)
                    {
                        if (child.name == gameObjectNameOrPathArray[i].Split('/')[0])
                        {
                            root = child;
                        }
                    }

                    string findStr = gameObjectNameOrPathArray[i].Remove(0, newRootName.Length + 1);
                    Transform _transform = root.Find(findStr);
                    if (_transform == null)
                    {
                        gameObjectList.Add(null);
                    }
                    else
                    {
                        gameObjectList.Add(_transform.gameObject);
                    }
                }
                else
                {// 是一個單元的名稱 ex.  "btn1"
                 //1.偵測是否有同名

                    if (allChildren.Count == 0)
                    {
                        DepthFirstSearchAllChildren(rootTransform, ref allChildren, true);
                    }

                    int objCount = 0;
                    Transform targetTransform = null;
                    foreach (Transform childTransfrom in allChildren)
                    {
                        if (childTransfrom.gameObject.name == gameObjectNameOrPathArray[i])
                        {
                            targetTransform = childTransfrom;
                            objCount++;
                        }
                    }

                    if (objCount > 1)
                    {
                        Debug.LogError($"UI [{rootTransform.name}]有多個同名的元件:{gameObjectNameOrPathArray[i]}");
                    }
                    else if (objCount == 0)
                    {
                        Debug.LogError($"UI [{rootTransform.name}]找不到元件:{gameObjectNameOrPathArray[i]}");
                    }
                    else
                    {
                        gameObjectList.Add(targetTransform.gameObject);
                    }
                }
            }

            //check是否為Null
            for (int i = 0; i < gameObjectList.Count; i++)
            {
                if (gameObjectList[i] == null)
                {
                    Debug.LogError($"{rootTransform.gameObject.name}執行GetGameObjects(),物件名稱 [{gameObjectNameOrPathArray[i]}]Find到的是NULL");
                }
                else
                {
                    if (findedCache[$"{rootTransform.gameObject.GetHashCode()}{gameObjectNameOrPathArray[i]}"] == null)
                    {
                        findedCache.Add($"{rootTransform.gameObject.GetHashCode()}{gameObjectNameOrPathArray[i]}", gameObjectList[i]);
                    }
                }
            }
            return gameObjectList.ToArray();
        }
        public static void DepthFirstSearchAllChildren(Transform targetTransform, ref List<Transform> transformList, bool includeRoot = false)
        {
            SearchAllChildren(targetTransform, ref transformList);
            if (includeRoot)
            {
                transformList.Add(targetTransform);
            }
        }
        private static void SearchAllChildren(Transform targetTransform, ref List<Transform> transformList)
        {
            foreach (Transform target in targetTransform)
            {
                transformList.Add(target);
                SearchAllChildren(target, ref transformList);
            }
        }
    }
}