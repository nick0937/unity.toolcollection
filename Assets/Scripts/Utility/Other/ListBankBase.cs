/*
 *  Project Name：philippines
 *
 *  Programmer：Nick Tseng
 *
 *  Start Date：2022/08/18
 *  
 *  UnityVersion：2020.3.15f2
 */

using System.Collections.Generic;

namespace Project.Utility.Other
{
    public abstract class ListBankBase<T>
    {
        #region Variable

        #endregion

        #region Public Method
        public abstract List<T> InitListBankDataList();
        public abstract int GetListLength();
        public abstract T GetListBankData(int index);
        public abstract List<T> LoopListBankDatas { get; }
        public abstract void SetListBankDatas(List<T> newDatas);
        #endregion
        #region Protected Method

        #endregion
        #region Private Method

        #endregion
    }
}