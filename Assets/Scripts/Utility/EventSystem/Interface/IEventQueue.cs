/*
 *  Project Name：philippines
 *
 *  Programmer：Nick Tseng
 *
 *  Start Date：2022/07/30
 *  
 *  UnityVersion：2020.3.15f2
 */

using System;

namespace Project.Utility.EventSystem
{
    public interface IEventQueue<TEnum, TArgs>
    {
        /// <summary>
        /// 訂閱事件
        /// </summary>
        /// <param name="e">事件列舉</param>
        /// <param name="listener">通知事件</param>
        void Subscribe(TEnum e, Action<TArgs> listener);
        /// <summary>
        /// 取消訂閱事件
        /// </summary>
        /// <param name="e">事件列舉</param>
        /// <param name="listener">通知事件</param>
        void UnSubscribe(TEnum e, Action<TArgs> listener);
        /// <summary>
        /// 發送訂閱事件
        /// </summary>
        /// <param name="e">事件列舉</param>
        /// <param name="args">通知事件資料</param>
        void SendNotify(TEnum e, TArgs args);
    }
}