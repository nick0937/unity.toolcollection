/*
 *  Project Name：Utility.Event
 *
 *  Programmer：Nick Tseng
 *
 *  Start Date：2022/08/05
 *  
 *  UnityVersion：2020.3.15f2
 */

namespace Utility.Event
{
    public interface IEventListener<T> : IEventBase
    {
        /// <summary>
        /// 事件觸發
        /// </summary>
        /// <param name="events"></param>
        void OnEventTrigger(T events);
    }
}