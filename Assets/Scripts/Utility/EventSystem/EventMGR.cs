/*
 *  Project Name：Utility.Event
 *
 *  Programmer：Nick Tseng
 *
 *  Start Date：2022/08/05
 *  
 *  UnityVersion：2020.3.15f2
 */

using System;
using System.Collections.Generic;

namespace Utility.Event
{
    public static class EventMGR
    {
        #region Variable
        private static Dictionary<Type, List<IEventBase>> queues;
        #endregion

        #region Public Method
        static EventMGR()
        {
            queues = new Dictionary<Type, List<IEventBase>>();
        }
        /// <summary>
        /// 註冊事件
        /// </summary>
        public static void Subscribe<TEvent>(IEventListener<TEvent> listener) where TEvent : struct
        {
            Type type = typeof(TEvent);

            if (!queues.ContainsKey(type))
                queues[type] = new List<IEventBase>();

            if (!ExistSubscribe(type, listener))
                queues[type].Add(listener);
        }
        /// <summary>
        /// 註銷事件
        /// </summary>
        public static void UnSubscribe<TEvent>(IEventListener<TEvent> listener) where TEvent : struct
        {
            Type type = typeof(TEvent);
            //檢查是否有Key
            if (!queues.ContainsKey(type)) return;

            List<IEventBase> eventBaseList = queues[type];

            for (int i = 0; i < eventBaseList.Count; i++)
            {
                if (eventBaseList[i] == listener)
                {
                    eventBaseList.Remove(eventBaseList[i]);

                    if (eventBaseList.Count == 0)
                    {
                        queues.Remove(type);
                    }
                    return;
                }
            }
        }
        public static void SendNotify<TEvent>(TEvent thisEvent) where TEvent : struct
        {
            List<IEventBase> eventBaseList;
            //檢查是否有Key
            if (!queues.TryGetValue(typeof(TEvent), out eventBaseList)) return;

            for (int num = 0; num < eventBaseList.Count; num++)
            {
                if (eventBaseList[num] is IEventListener<TEvent>)
                    (eventBaseList[num] as IEventListener<TEvent>).OnEventTrigger(thisEvent);
            }
        }
        /// <summary>
        /// 註銷全部事件
        /// </summary>
        public static void UnSubscribeAll()
        {
            queues.Clear();
        }
        #endregion
        #region Protected Method

        #endregion
        #region Private Method
        /// <summary>
        /// 檢查是否重複註冊
        /// </summary>
        private static bool ExistSubscribe(Type type, IEventBase listener)
        {
            List<IEventBase> eventBaseList;
            //檢查是否有Key
            if (!queues.TryGetValue(type, out eventBaseList)) return false;

            bool isExist = false;

            for (int i = 0; i < eventBaseList.Count; i++)
            {
                if (eventBaseList[i] == listener)
                {
                    isExist = true;
                    break;
                }
            }

            return isExist;
        }
        #endregion
    }
}