﻿using Utility.Singleton;

public class DemoClass : Singleton<DemoClass>
{
    public string AssetPath;
    public string AssetName;
}
