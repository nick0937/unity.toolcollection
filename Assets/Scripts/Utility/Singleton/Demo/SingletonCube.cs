﻿using Utility.Singleton;

public class SingletonCube : MonoSingleton<SingletonCube>
{
    protected override void OnAwake()
    {
        base.OnAwake();
        Debug.LogWarning("..........SingletonSample.OnAwake()");
    }
    protected override string GetSingletonName()
    {
        return "SingletonMonoBehaviour";
    }
    protected override bool IsDestroyOnLoad()
    {
        return true;
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        Debug.LogWarning("..........SingletonSample.OnDestroy()");
    }
    #region Public
    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
    public void Destroy()
    {
        Destroy(gameObject);
    }
    #endregion
}
