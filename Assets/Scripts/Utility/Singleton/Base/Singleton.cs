namespace Utility.Singleton
{
    public class Singleton<T> where T : class, new()
    {
        public static T Inst
        {
            get
            {

                lock (_locks)
                {
                    if (_inst == null)
                    {
                        _inst = new T();
                    }

                    return _inst;
                }
            }
        }
        private static T _inst;
        private static object _locks = new object();
    }
}