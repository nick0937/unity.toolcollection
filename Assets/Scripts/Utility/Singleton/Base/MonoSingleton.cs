using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utility.Extensions;
using Utility.Other;

namespace Utility.Singleton
{
    public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        #region Singleton Logic
        public static T Inst
        {
            get
            {
                lock (_locks)
                {
                    if (_inst == null)
                    {
                        T[] managers = FindObjectsOfType(typeof(T)) as T[];
                        if (managers.Length != 0)
                        {
                            if (managers.Length == 1)
                            {//如已存在Hierarchy，更名後回傳。
                                _inst = managers[0];
                                return _inst;
                            }
                            else
                            {//如Hierarchy大於兩個，Del All。
                                Debug.LogError($"You have more than one {typeof(T).Name} in the scene.");
                                foreach (T manager in managers)
                                {
                                    Destroy(manager.gameObject);
                                }
                            }
                        }
                        Create();   //創建新的物件
                    }
                }
                return _inst;
            }
        }
        private static T _inst;
        private static object _locks = new object();
        /// <summary>
        /// 建立Singleton Object
        /// </summary>
        private static void Create()
        {
            try
            {
                GameObject singletonObj;
                singletonObj = new GameObject();
                _inst = singletonObj.GetOrAddComponent<T>();
            }
            catch (Exception ex)
            {
                Debug.LogError($"{ex.GetType().Name}: {ex.Message}");
            }
        }
        #endregion
        #region Mandatory Implementation
        /// <summary>
        /// 取得Singleton 名子
        /// </summary>
        protected abstract string GetSingletonName();
        /// <summary>
        /// 是否過場刪除
        /// </summary>
        protected virtual bool IsDestroyOnLoad()
        {
            return true;
        }
        protected string SceneName;
        /// <summary>
        /// 是否要搬移到指定場景
        /// </summary>
        /// <returns></returns>
        protected virtual bool IsMoveToTargetScene()
        {
            return false;
        }
        #endregion
        #region Initialization Logic
        private void Awake()
        {
            if (IsDestroyOnLoad() == false)
            {
                gameObject.DontDestroyOnLoad();
            }
            else if (IsMoveToTargetScene())
            {
                SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetSceneByName(SceneName));
            }
            OnAwake();
        }
        protected virtual void OnAwake()
        {
            gameObject.name = $"{GetSingletonName()}(Singleton)";
        }
        private void Start()
        {
            OnStart();
        }
        protected virtual void OnStart()
        {

        }
        protected virtual void OnDestroy()
        {
            _inst = null;
        }
        #endregion
    }
}