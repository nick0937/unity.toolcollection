using Cysharp.Threading.Tasks;
using NickABC.Utils.UI;
using System.Collections;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using Utility.Extensions;
using Utility.Other;

namespace Utility.MessageBox
{
    public class MessageBoxExample : MonoBehaviour
    {
        private Text txt_Title;
        private Text txt_Body;
        private GameObject btn_Close;
        private GameObject btn_Confirm;
        private GameObject btn_Cancel;

        private string str_title;
        private string str_body;

        public BoxResult Result { get; private set; }
        private Hashtable hashtable = new Hashtable();
        private CancellationTokenSource cts = new CancellationTokenSource();
        void Awake()
        {
            Initialize();
        }
        #region Public Method
        public void SetContent(string title, string body, bool showBtnClose, bool showBtnConfirm, bool showBtnCancel)
        {
            txt_Title.text = title;
            txt_Body.text = body;
            btn_Close.SetActive(showBtnClose);
            btn_Confirm.SetActive(showBtnConfirm);
            btn_Cancel.SetActive(showBtnCancel);
        }
        public async UniTask<BoxResult> GetReplyAsync()
        {
            return await UniTask.RunOnThreadPool(() =>
            {
                while (true)
                {
                    if (Result != BoxResult.None)
                    {
                        return Result;
                    }
                    UniTask.Yield();
                }
            }, cancellationToken: cts.Token);
        }
        public void ResetData()
        {
            txt_Title.text = string.Empty;
            txt_Body.text = string.Empty;
            btn_Close.SetActive(false);
            btn_Confirm.SetActive(false);
            btn_Cancel.SetActive(false);

            Result = BoxResult.None;

            cts.Cancel();
            cts.Dispose();
            cts = new CancellationTokenSource();
        }
        #endregion
        #region Private Method
        private void Initialize()
        {
            txt_Title = Utilities.Select(hashtable, transform, "txt_Title").GetOrAddComponent<Text>();
            txt_Body = Utilities.Select(hashtable, transform, "txt_Body").GetOrAddComponent<Text>();

            btn_Close = Utilities.Select(hashtable, transform, "btn_Close");
            btn_Close.GetOrAddComponent<ClickEvent>().OnClickCall = () => { Result = BoxResult.Close; };

            btn_Confirm = Utilities.Select(hashtable, transform, "btn_Confirm");
            btn_Confirm.GetOrAddComponent<ClickEvent>().OnClickCall = () => { Result = BoxResult.Confirm; };

            btn_Cancel = Utilities.Select(hashtable, transform, "btn_Cancel");
            btn_Cancel.GetOrAddComponent<ClickEvent>().OnClickCall = () => { Result = BoxResult.Cancel; };
        }
        #endregion
        public enum BoxResult
        {
            None,
            Close,
            Confirm,
            Cancel
        }
    }
}
