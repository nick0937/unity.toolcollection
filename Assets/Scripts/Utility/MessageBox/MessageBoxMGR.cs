using Cysharp.Threading.Tasks;
using System.Collections;
using UnityEngine;
using Utility.Other;

namespace Utility.MessageBox
{
    public class MessageBoxMGR : MonoBehaviour
    {
        [SerializeField]
        private GameObject _messageBox;
        [SerializeField]
        private int _boxCount = 5;
        private static bool _isInitialize = false;

        private GameObject _messageBoxTarget;


        private static ObjectPool<MessageBoxExample> _messageBoxPool;
        private Hashtable _hashtable = new Hashtable();

        void Awake()
        {
            if (_messageBox != null)
            {
                _messageBoxTarget = Utilities.Select(_hashtable, transform, "MessageBoxTarget");
                _messageBoxPool = new ObjectPool<MessageBoxExample>(prefab: _messageBox, startSize: _boxCount, parent: _messageBoxTarget.transform);
                _isInitialize = true;
            }
        }
        private void OnDestroy()
        {
            CloseAll();
        }
        #region Public Method
        /// <summary>
        /// 顯示提交類型訊息視窗
        /// </summary>
        /// <param name="body"></param>
        /// <returns>MessageBox</returns>
        public static MessageBoxExample ShowSubmitWindow(string body)
        {
            return BuildMessageBox("", body, false, true, true);
        }
        /// <summary>
        /// 顯示提交類型訊息視窗
        /// </summary>
        /// <param name="title">標題</param>
        /// <param name="body"></param>
        /// <returns>MessageBox</returns>
        public static MessageBoxExample ShowSubmitWindow(string title, string body)
        {
            return BuildMessageBox(title, body, false, true, true);
        }
        /// <summary>
        /// 顯示提交類型訊息視窗
        /// </summary>
        /// <param name="title">標題</param>
        /// <param name="body"></param>
        /// <returns>MessageBox</returns>
        public static MessageBoxExample ShowOKWindow(string title, string body)
        {
            return BuildMessageBox(title, body, false, true, false);
        }
        /// <summary>
        /// 顯示提交訊息類型視窗-使用預設非同步處理流程，回傳按鈕點擊結果。
        /// </summary>
        /// <param name="title">標題</param>
        /// <param name="body">內容</param>
        /// <returns>BoxResult</returns>
        public static async UniTask<MessageBoxExample.BoxResult> ShowSubmitWindowResult(string title = "", string body = "")
        {
            MessageBoxExample box = ShowSubmitWindow(title, body);
            MessageBoxExample.BoxResult Result = await box.GetReplyAsync();
            Close(box);
            return Result;
        }
        /// <summary>
        /// 顯示提交訊息類型視窗-使用預設非同步處理流程，回傳按鈕點擊結果。
        /// </summary>
        /// <param name="title">標題</param>
        /// <param name="body">內容</param>
        /// <returns>BoxResult</returns>
        public static async UniTask<MessageBoxExample.BoxResult> ShowOKWindowResult(string title = "", string body = "")
        {
            MessageBoxExample box = ShowOKWindow(title, body);
            MessageBoxExample.BoxResult Result = await box.GetReplyAsync();
            Close(box);
            return Result;
        }
        /// <summary>
        /// 顯示系統訊息類型視窗
        /// </summary>
        /// <param name="body">內容</param>
        /// <returns>MessageBox</returns>
        public static MessageBoxExample ShowSystemInfoWindow(string body)
        {
            return BuildMessageBox("", body, true, false, false);
        }
        /// <summary>
        /// 顯示系統訊息類型視窗
        /// </summary>
        /// <param name="title">標題</param>
        /// <param name="body">內容</param>
        /// <returns>MessageBox</returns>
        public static MessageBoxExample ShowSystemInfoWindow(string title, string body)
        {
            return BuildMessageBox(title, body, true, false, false);
        }
        /// <summary>
        /// 顯示系統訊息類型視窗-使用預設非同步處理流程，回傳按鈕點擊結果。
        /// </summary>
        /// <param name="title">標題</param>
        /// <param name="body">內容</param>
        /// <returns>BoxResult</returns>
        public static async UniTask<MessageBoxExample.BoxResult> ShowSystemInfoResult(string title = "", string body = "")
        {
            MessageBoxExample box = ShowSystemInfoWindow(title, body);
            MessageBoxExample.BoxResult Result = await box.GetReplyAsync();
            Close(box);
            return Result;
        }
        /// <summary>
        /// 顯示系統訊息類型視窗-只顯示。
        /// </summary>
        /// <param name="title">標題</param>
        /// <param name="body">內容</param>
        public static async void ShowSystemInfoOnly(string title = "", string body = "")
        {
            await ShowSystemInfoResult(title, body);
        }
        /// <summary>
        /// 顯示訊息視窗。
        /// </summary>
        /// <param name="title">標題</param>
        /// <param name="body">內容</param>
        /// <param name="showBtnClose">是否顯示關閉按鈕</param>
        /// <param name="showBtnConfirm">是否顯示確認按鈕</param>
        /// <param name="showBtnCancel">是否顯示取消按鈕</param>
        /// <returns>MessageBox</returns>
        public static MessageBoxExample BuildMessageBox(string title, string body, bool showBtnClose, bool showBtnConfirm, bool showBtnCancel)
        {
            if (!_isInitialize)
            {
                UnityEngine.Debug.LogError("The MessageBoxMGR has not been initialized!");
                return null;
            }
            MessageBoxExample box;
            box = _messageBoxPool.Spawn();
            box.SetContent(title, body, showBtnClose, showBtnConfirm, showBtnCancel);
            box.gameObject.SetActive(true);
            return box;
        }
        /// <summary>
        /// 關閉訊息視窗
        /// </summary>
        /// <param name="box"></param>
        public static void Close(MessageBoxExample box)
        {
            box.ResetData();
            _messageBoxPool.Despawn(box);
        }
        /// <summary>
        /// 關閉所有訊息視窗
        /// </summary>
        public static void CloseAll()
        {
            foreach (var box in _messageBoxPool.GetInUsePoolList())
            {
                box.typeObj.ResetData();
            }
            _messageBoxPool.ResetPoolAll();
        }
        public static bool GetInitialize()
        {
            return _isInitialize;
        }
        #endregion
        #region Private Method

        #endregion
    }
}
