using Cysharp.Threading.Tasks;
using PublicTool.GameData.Config;
using PublicTool.GameData.Data;
using System;
using System.Collections.Generic;
using UnityEngine;
namespace PublicTool.GameData
{
    public class GameDataMGR : MonoBehaviour, IGameService
    {
        public bool IsInitializing { get; private set; } = true;

        #region EventAction
        #region AppInfo
        public event Action OnSaveGameVersion = null;
        public event Action<string> OnLoadGameVersion = null;

        public event Action OnSaveInviteCode = null;
        public event Action<string> OnLoadInviteCode = null;

        public event Action OnSaveDeviceID = null;
        public event Action<string> OnLoadDeviceID = null;

        public event Action OnSavePipes = null;
        public event Action<Pipe> OnLoadPipes = null;
        #endregion
        #region LoginInfo
        public event Action OnSaveLoginInfo = null;
        public event Action<LoginInfo> OnLoadLoginInfo = null;
        #endregion
        #region Preference
        public event Action OnSaveAudioSetting = null;
        public event Action<AudioSettingData> OnLoadAudioSetting = null;

        public event Action OnSaveLanguageSetting = null;
        public event Action<LanguageSettingData> OnLoadLanguageSetting = null;

        public event Action OnSaveProfilePhoto = null;
        public event Action<ProfilePhoto> OnLoadProfilePhoto = null;

        public event Action OnSaveNotificationEnabled = null;
        public event Action<bool> OnLoadNotificationEnabled = null;

        public event Action OnSaveVibrateEnabled = null;
        public event Action<bool> OnLoadVibrateEnabled = null;
        #endregion
        #region Initialize
        public event Action OnInitializeComplete = null;
        #endregion
        #endregion
        #region Config
        private AppConfig appConfig;
        private LoginInfoConfig loginInfoConfig;
        private PreferencesConfig preferencesConfig;
        #endregion
        public async virtual void Initialize()
        {
            appConfig = new AppConfig();
            loginInfoConfig = new LoginInfoConfig();
            preferencesConfig = new PreferencesConfig();

            await InitAppInfo();
            await InitLoginInfo();
            await InitPreferenceInfo();
            OnInitializeComplete?.Invoke();
        }
        #region AppInfo Method
        #region AppInfo
        private async UniTask InitAppInfo()
        {
            FBPP.Start(appConfig.config);
            await LoadGameVersion();
            await LoadInviteCode();
            await LoadDeviceID();
            await LoadPipes();
        }
        public AppInfo LoadAppInfo()
        {
            return appConfig.value;
        }
        #endregion
        #region GameVersion
        public void SetGameVersion(string ver, bool autoSave = true)
        {
            appConfig.value.Version = ver;

            if (autoSave)
                SaveGameVersion();
        }
        public void SaveGameVersion()
        {
            FBPP.Start(appConfig.config);
            FBPP.SetString(AppConfig.KVersion, appConfig.value.Version);
            OnSaveGameVersion?.Invoke();
        }
        public async UniTask LoadGameVersion()
        {
            FBPP.Start(appConfig.config);
            if (FBPP.HasKeyForString(AppConfig.KVersion))
            {
                await UniTask.Yield();
                var data = FBPP.GetString(AppConfig.KVersion);
                appConfig.value.Version = data;
                OnLoadGameVersion?.Invoke(data);
                return;
            }
        }
        #endregion
        #region InviteCode
        public void SetInviteCode(string code, bool autoSave = true)
        {
            appConfig.value.InviteCode = code;

            if (autoSave)
                SaveInviteCode();
        }
        public void SaveInviteCode()
        {
            FBPP.Start(appConfig.config);
            FBPP.SetString(AppConfig.KInviteCode, appConfig.value.InviteCode);
            OnSaveInviteCode?.Invoke();
        }
        public async UniTask LoadInviteCode()
        {
            FBPP.Start(appConfig.config);
            if (FBPP.HasKeyForString(AppConfig.KInviteCode))
            {
                await UniTask.Yield();
                var data = FBPP.GetString(AppConfig.KInviteCode);
                appConfig.value.InviteCode = data;
                OnLoadInviteCode?.Invoke(data);
            }
        }
        #endregion
        #region DeviceID
        public void SetDeviceID(string id, bool autoSave = true)
        {
            appConfig.value.DeviceID = id;

            if (autoSave)
                SaveDeviceID();
        }
        public void SaveDeviceID()
        {
            FBPP.Start(appConfig.config);
            FBPP.SetString(AppConfig.KDeviceID, appConfig.value.DeviceID);
            OnSaveDeviceID?.Invoke();
        }
        public async UniTask LoadDeviceID()
        {
            FBPP.Start(appConfig.config);
            if (FBPP.HasKeyForString(AppConfig.KDeviceID))
            {
                await UniTask.Yield();
                var data = FBPP.GetString(AppConfig.KDeviceID);
                appConfig.value.DeviceID = data;
                OnLoadDeviceID?.Invoke(data);
            }
        }
        #endregion
        #region Pipes
        public void AddPipes(string url, bool autoSave = true)
        {
            appConfig.value.Pipe.PipeList.Add(url);

            if (autoSave)
                SavePipes();
        }
        public void AddPipes(List<string> list, bool autoSave = true)
        {
            appConfig.value.Pipe.PipeList.AddRange(list);

            if (autoSave)
                SavePipes();
        }
        public void UpdatePipes(List<string> list, bool autoSave = true)
        {
            appConfig.value.Pipe.PipeList = list;

            if (autoSave)
                SavePipes();
        }
        public void SavePipes()
        {
            FBPP.Start(appConfig.config);
            FBPP.SetString(AppConfig.KPipes, appConfig.GetPipesJson());
            OnSavePipes?.Invoke();
        }
        public async UniTask LoadPipes()
        {
            FBPP.Start(appConfig.config);
            if (FBPP.HasKeyForString(AppConfig.KPipes))
            {
                await UniTask.Yield();
                var json = FBPP.GetString(AppConfig.KPipes);
                var data = appConfig.GetConnectList(json);
                if (data != null)
                {
                    appConfig.value.Pipe = data;
                }
                OnLoadPipes?.Invoke(data);
            }
        }
        #endregion
        #endregion
        #region LoginInfo Method
        public async UniTask InitLoginInfo()
        {
            FBPP.Start(loginInfoConfig.config);
            await LoadLoginInfo();
        }
        public void SetLoginInfo(LoginInfo info, bool autoSave = true)
        {
            loginInfoConfig.value = info;

            if (autoSave)
                SaveLoginInfo();
        }
        public void SaveLoginInfo()
        {
            FBPP.Start(loginInfoConfig.config);
            FBPP.SetString(LoginInfoConfig.KLoginInfo, loginInfoConfig.GetJsonData());
            OnSaveLoginInfo?.Invoke();
        }
        public async UniTask LoadLoginInfo()
        {
            FBPP.Start(loginInfoConfig.config);
            if (FBPP.HasKeyForString(LoginInfoConfig.KLoginInfo))
            {
                await UniTask.Yield();
                var json = FBPP.GetString(LoginInfoConfig.KLoginInfo);
                var data = loginInfoConfig.Deserialize(json);
                if (data != null)
                {
                    loginInfoConfig.value = data;
                }
                OnLoadLoginInfo?.Invoke(data);
            }
            else
            {
                OnLoadLoginInfo?.Invoke(null);
            }
        }
        #endregion
        #region Preference Method
        #region PreferencesInfo
        private async UniTask InitPreferenceInfo()
        {
            FBPP.Start(preferencesConfig.config);
            await LoadAudioSetting();
            await LoadLanguageSetting();
            await LoadProfilePhoto();
            await LoadNotificationEnabled();
            await LoadVibrateEnabled();
        }
        public PreferencesInfo LoadPreferenceInfo()
        {
            return preferencesConfig.value;
        }
        #endregion
        #region Audio
        public void SetMusic(int current, bool autoSave = true)
        {
            preferencesConfig.value.AudioSettingData.Current = Mathf.Clamp(current, 0, 3);

            if (autoSave)
                SaveAudioSetting();
        }
        public void SetMusicVolume(float vol, bool autoSave = true)
        {
            preferencesConfig.value.AudioSettingData.MusicVolume = Mathf.Clamp(vol, 0.0f, 1.0f); ;

            if (autoSave)
                SaveAudioSetting();
        }
        public void SetSoundEffectVolume(float vol, bool autoSave = true)
        {
            preferencesConfig.value.AudioSettingData.SoundEffectVolume = Mathf.Clamp(vol, 0.0f, 1.0f); ;

            if (autoSave)
                SaveAudioSetting();
        }
        public void SetMusicEnabled(bool enabled, bool autoSave = true)
        {
            preferencesConfig.value.AudioSettingData.MusicEnabled = enabled;

            if (autoSave)
                SaveAudioSetting();
        }
        public void SetSoundEffectEnabled(bool enabled, bool autoSave = true)
        {
            preferencesConfig.value.AudioSettingData.SoundEffectEnabled = enabled;

            if (autoSave)
                SaveAudioSetting();
        }
        public void SetAudioSetting(AudioSettingData data, bool autoSave = true)
        {
            preferencesConfig.value.AudioSettingData = data;

            if (autoSave)
                SaveAudioSetting();
        }
        public void SaveAudioSetting()
        {
            FBPP.Start(preferencesConfig.config);
            FBPP.SetString(PreferencesConfig.KAudioSetting, preferencesConfig.GetAudioSettingJson());
            OnSaveAudioSetting?.Invoke();
        }
        public async UniTask LoadAudioSetting()
        {
            FBPP.Start(preferencesConfig.config);
            if (FBPP.HasKeyForString(PreferencesConfig.KAudioSetting))
            {
                await UniTask.Yield();
                var json = FBPP.GetString(PreferencesConfig.KAudioSetting);
                var data = preferencesConfig.Deserialize<AudioSettingData>(json);
                preferencesConfig.value.AudioSettingData = data;
                OnLoadAudioSetting?.Invoke(data);
            }
        }
        #endregion
        #region Language
        public void SetLanguageSetting(LanguageSettingData data, bool autoSave = true)
        {
            preferencesConfig.value.LanguageSettingData = data;

            if (autoSave)
                SaveLanguageSetting();
        }
        public void SaveLanguageSetting()
        {
            FBPP.Start(preferencesConfig.config);
            FBPP.SetString(PreferencesConfig.KLanguageSetting, preferencesConfig.GetLanguageSetting());
            OnSaveLanguageSetting?.Invoke();
        }
        public async UniTask LoadLanguageSetting()
        {
            FBPP.Start(preferencesConfig.config);
            if (FBPP.HasKeyForString(PreferencesConfig.KLanguageSetting))
            {
                await UniTask.Yield();
                var json = FBPP.GetString(PreferencesConfig.KLanguageSetting);
                var data = preferencesConfig.Deserialize<LanguageSettingData>(json);
                preferencesConfig.value.LanguageSettingData = data;
                OnLoadLanguageSetting?.Invoke(data);
            }
        }
        #endregion
        #region ProfilePhoto
        public void SetLanguageSetting(ProfilePhoto data, bool autoSave = true)
        {
            preferencesConfig.value.ProfilePhoto = data;

            if (autoSave)
                SaveProfilePhoto();
        }
        public void SaveProfilePhoto()
        {
            FBPP.Start(preferencesConfig.config);
            FBPP.SetString(PreferencesConfig.KProfilePhoto, preferencesConfig.GetProfilePhoto());
            OnSaveProfilePhoto?.Invoke();
        }
        public async UniTask LoadProfilePhoto()
        {
            FBPP.Start(preferencesConfig.config);
            if (FBPP.HasKeyForString(PreferencesConfig.KProfilePhoto))
            {
                await UniTask.Yield();
                var json = FBPP.GetString(PreferencesConfig.KProfilePhoto);
                var data = preferencesConfig.Deserialize<ProfilePhoto>(json);
                preferencesConfig.value.ProfilePhoto = data;
                OnLoadProfilePhoto?.Invoke(data);
            }
        }
        #endregion
        #region Notification
        public void SetNotificationEnabled(bool endabled, bool autoSave = true)
        {
            preferencesConfig.value.NotificationEnabled = endabled;

            if (autoSave)
                SaveNotificationEnabled();
        }
        public void SaveNotificationEnabled()
        {
            FBPP.SetBool(PreferencesConfig.KNotificationEnabled, preferencesConfig.value.NotificationEnabled);
            OnSaveNotificationEnabled?.Invoke();
        }
        public async UniTask LoadNotificationEnabled()
        {
            FBPP.Start(preferencesConfig.config);
            if (FBPP.HasKeyForBool(PreferencesConfig.KNotificationEnabled))
            {
                await UniTask.Yield();
                var data = FBPP.GetBool(PreferencesConfig.KNotificationEnabled);
                preferencesConfig.value.NotificationEnabled = data;
                OnLoadNotificationEnabled?.Invoke(data);
                return;
            }
        }
        #endregion
        #region Vibrate
        public void SetVibrateEnabled(bool endabled, bool autoSave = true)
        {
            preferencesConfig.value.VibrateEnabled = endabled;

            if (autoSave)
                SaveVibrateEnabled();
        }
        public void SaveVibrateEnabled()
        {
            FBPP.SetBool(PreferencesConfig.KVibrateEnabled, preferencesConfig.value.VibrateEnabled);
            OnSaveVibrateEnabled?.Invoke();
        }
        public async UniTask LoadVibrateEnabled()
        {
            FBPP.Start(preferencesConfig.config);
            if (FBPP.HasKeyForBool(PreferencesConfig.KVibrateEnabled))
            {
                await UniTask.Yield();
                var data = FBPP.GetBool(PreferencesConfig.KVibrateEnabled);
                preferencesConfig.value.VibrateEnabled = data;
                OnLoadVibrateEnabled?.Invoke(data);
                return;
            }
        }
        #endregion
        #endregion
    }
}
