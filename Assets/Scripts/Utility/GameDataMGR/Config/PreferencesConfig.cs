using PublicTool.GameData.Data;

public class PreferencesConfig : DataConfig<PreferencesInfo, DataType>
{
    public PreferencesConfig() : base(DataType.Preference)
    {
        value.AudioSettingData = new AudioSettingData();
        value.LanguageSettingData = new LanguageSettingData();
        value.ProfilePhoto = new ProfilePhoto();
    }
    /// <summary>
    /// DataKey-音樂設定
    /// </summary>
    public const string KAudioSetting = "audioSetting";
    /// <summary>
    /// DataKey-語言設定
    /// </summary>
    public const string KLanguageSetting = "languageSetting";
    /// <summary>
    /// DataKey-頭像設定
    /// </summary>
    public const string KProfilePhoto = "profilePhoto";
    /// <summary>
    /// DataKey-通知開關
    /// </summary>
    public const string KNotificationEnabled = "notificationEnabled";
    /// <summary>
    /// DataKey-震動開關
    /// </summary>
    public const string KVibrateEnabled = "vibrateEnabled";
    /// <summary>
    /// 取得音樂設定-Json格式
    /// </summary>
    public string GetAudioSettingJson() => Serialize(value.AudioSettingData);
    /// <summary>
    /// 取得語言設定-Json格式
    /// </summary>
    public string GetLanguageSetting() => Serialize(value.LanguageSettingData);
    /// <summary>
    /// 取得頭像設定-Json格式
    /// </summary>
    public string GetProfilePhoto() => Serialize(value.ProfilePhoto);
}
