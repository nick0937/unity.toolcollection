using PublicTool.GameData.Data;
using System.Collections.Generic;

namespace PublicTool.GameData.Config
{
    /// <summary>
    /// 連線配置
    /// </summary>
    public class AppConfig : DataConfig<AppInfo, DataType>
    {
        public AppConfig() : base(DataType.AppInfo)
        {
            value.Version = "0";
            value.InviteCode = "";
            value.Pipe = new Pipe();
        }
        /// <summary>
        /// DataKey-版號
        /// </summary>
        public const string KVersion = "version";
        /// <summary>
        /// DataKey-邀請碼
        /// </summary>
        public const string KInviteCode = "inviteCode";
        /// <summary>
        /// DataKey-手機序號
        /// </summary>
        public const string KDeviceID = "deviceID";
        /// <summary>
        /// DataKey-線路清單
        /// </summary>
        public const string KPipes = "pipes";
        /// <summary>
        /// 取得連線線路列表-Json格式
        /// </summary>
        public string GetPipesJson() => Serialize(value.Pipe);
        /// <summary>
        /// 取得連線線路列表-Json=>List格式
        /// </summary>
        public Pipe GetConnectList(string list)
        {
            return Deserialize<Pipe>(list);
        }
    }
}
