using System;
using Utility.Extensions;
using Utility.Tool;

public class DataConfig<T, TEnum> : JsonSerializationOption where T : class, new() where TEnum : Enum
{
    public DataConfig(TEnum type, bool autoInit = true)
    {
        dataType = type;
        value = new T();

        if (autoInit)
            Initialize();
    }
    /// <summary>
    /// FBPP配置
    /// </summary>
    public FBPPConfig config;
    /// <summary>
    /// 儲存資料
    /// </summary>
    public T value;
    /// <summary>
    /// 儲存檔案名稱
    /// </summary>
    public string FileName
    {
        get { return dataType.GetDescription(); }
    }
    /// <summary>
    /// 儲存副檔名
    /// </summary>
    public FileNameExtension FileNameExtension;
    /// <summary>
    /// 資料類型
    /// </summary>
    private TEnum dataType;
    /// <summary>
    /// 序列化<T> Json格式
    /// </summary>
    public string GetJsonData() => Serialize(value);
    /// <summary>
    /// 反序列化 <T>
    /// </summary>
    public virtual T Deserialize(string data)
    {
        return Deserialize<T>(data);
    }
    /// <summary>
    /// 初始化配置
    /// </summary>
    public virtual void Initialize()
    {
        config = new FBPPConfig();
        config.ScrambleSaveData = false;
        config.SaveFileName = $"{FileName}{FileNameExtension.GetDescription()}";
        config.OnLoadError = OnLoadError;
    }
    /// <summary>
    /// 讀取錯誤
    /// </summary>
    public virtual void OnLoadError()
    {
        Debug.LogError($"DataConfig Type: {dataType}...OnLoadError");
        FBPP.OverwriteLocalSaveFile(FBPP.GetSaveFileAsJson());
    }
}
