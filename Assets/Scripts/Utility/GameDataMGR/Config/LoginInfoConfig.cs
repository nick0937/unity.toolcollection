using PublicTool.GameData.Data;
using UnityEngine;

namespace PublicTool.GameData.Config
{
    /// <summary>
    /// 登入配置
    /// </summary>
    public class LoginInfoConfig : DataConfig<LoginInfo, DataType>
    {
        public LoginInfoConfig(): base(DataType.LoginInfo)
        {
            
        }
        public const string KLoginInfo = "loginInfo";
    }
}
