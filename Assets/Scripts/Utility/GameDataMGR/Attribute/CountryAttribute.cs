using System;

public sealed class CountryAttribute : Attribute
{
    /// <summary>
    /// 國家名稱
    /// </summary>
    public string CountryName { get; private set; }
    /// <summary>
    /// 手機國際碼
    /// </summary>
    public string PhoneCode { get; private set; }
    /// <summary>
    /// 設定國家內容
    /// </summary>
    /// <param name="name">國家名稱</param>
    /// <param name="code">手機國際碼</param>
    public CountryAttribute(string name, string code)
    {
        CountryName = name;
        PhoneCode = code;
    }
}
