using System.ComponentModel;

public enum Country
{
    [Country("菲律賓", "+63")]
    PH,
    [Country("美國", "+1")]
    US
}
