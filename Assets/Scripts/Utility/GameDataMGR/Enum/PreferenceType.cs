using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PreferenceType
{
    Audio,
    Notification,
    Vibrate,
    Language,
    ProfilePhoto
}
