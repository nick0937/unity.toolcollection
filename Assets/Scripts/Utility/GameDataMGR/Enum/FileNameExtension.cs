using System.ComponentModel;

public enum FileNameExtension
{
    [Description(".txt")]
    TXT,
    [Description(".json")]
    JSON,
    [Description(".sav")]
    SAV
}
