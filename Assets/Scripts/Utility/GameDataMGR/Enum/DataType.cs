using System.ComponentModel;

public enum DataType
{
    [Description("AppInfo")]
    AppInfo,
    [Description("LoginInfo")]
    LoginInfo,
    [Description("Preference")]
    Preference,
}
