using System;
using System.Collections.Generic;

namespace PublicTool.GameData.Data
{
    [Serializable]
    /// <summary>
    /// App資料
    /// </summary>
    public class AppInfo
    {
        /// <summary>
        /// 版本號
        /// </summary>
        public string Version;
        /// <summary>
        /// 邀請碼
        /// </summary>
        public string InviteCode;
        /// <summary>
        /// 手機序號
        /// </summary>
        public string DeviceID;
        /// <summary>
        /// 線路列表
        /// </summary>
        public Pipe Pipe;
    }
    public class Pipe
    {
        /// <summary>
        /// 線路列表
        /// </summary>
        public List<string> PipeList;
    }
}
