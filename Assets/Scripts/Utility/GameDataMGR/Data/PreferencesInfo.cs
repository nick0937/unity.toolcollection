using System;
namespace PublicTool.GameData.Data
{
    [Serializable]
    public class PreferencesInfo
    {
        public bool NotificationEnabled;
        public bool VibrateEnabled;
        public AudioSettingData AudioSettingData;
        public LanguageSettingData LanguageSettingData;
        public ProfilePhoto ProfilePhoto;
    }
}
