using System;
using System.Collections.Generic;
namespace PublicTool.GameData.Data
{
    [Serializable]
    /// <summary>
    /// 頭像資料
    /// </summary>
    public class ProfilePhoto
    {
        /// <summary>
        /// 目前頭像
        /// </summary>
        public int Current;
        /// <summary>
        /// 頭像名稱列表
        /// </summary>
        public List<string> PhotoList = new List<string>();
    }
}
