using System;
using System.Collections.Generic;
namespace PublicTool.GameData.Data
{
    [Serializable]
    /// <summary>
    /// 語系設定資料
    /// </summary>
    public class LanguageSettingData
    {
        /// <summary>
        /// 目前語言
        /// </summary>
        public int Current;
        /// <summary>
        /// 語言列表
        /// </summary>
        public List<LanguageData> LangList = new List<LanguageData>()
        {
            new LanguageData(){ LangCultureName = "en-US", DisplayName = "English"},
            new LanguageData(){ LangCultureName = "en-PH", DisplayName = "Filipino"}
        };
        public class LanguageData
        {
            /// <summary>
            /// 語言名稱
            /// </summary>
            public string LangCultureName;
            /// <summary>
            /// 顯示名稱
            /// </summary>
            public string DisplayName;
        }
    }
}
