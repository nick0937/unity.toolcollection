using System;
using Utility.Extensions;

namespace PublicTool.GameData.Data
{
    [Serializable]
    /// <summary>
    /// 登入資料
    /// </summary>
    public class LoginInfo
    {
        /// <summary>
        /// 自動儲存
        /// </summary>
        public bool AutoSave = true;
        /// <summary>
        /// 自動登入
        /// </summary>
        public bool AutoLogin = true;
        /// <summary>
        /// 國家代碼
        /// </summary>
        public Country CountryCode;
        /// <summary>
        /// 國家名稱
        /// </summary>
        public string CountryName { get { return CountryCode.GetDescription<CountryAttribute>().CountryName; } }
        /// <summary>
        /// 手機國際碼
        /// </summary>
        public string PhoneCode { get { return CountryCode.GetDescription<CountryAttribute>().PhoneCode; } }
        /// <summary>
        /// 手機號碼
        /// </summary>
        public string PhoneNumber;
        /// <summary>
        /// 登入密碼
        /// </summary>
        public string Password;
        /// <summary>
        /// 手機編號
        /// </summary>
        public string DeviceID;
        /// <summary>
        /// 登入Token(自動登入使用)
        /// </summary>
        public string Token;
    }
}
