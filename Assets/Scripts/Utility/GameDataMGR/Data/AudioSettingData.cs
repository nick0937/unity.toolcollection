using System;

namespace PublicTool.GameData.Data
{
    [Serializable]
    /// <summary>
    /// 聲音設定資料
    /// </summary>
    public class AudioSettingData
    {
        /// <summary>
        /// 目前曲目順序
        /// </summary>
        public int Current = 0;
        /// <summary>
        /// 音樂音量
        /// </summary>
        public float MusicVolume = 1.0f;
        /// <summary>
        /// 音效音量
        /// </summary>
        public float SoundEffectVolume = 1.0f;
        /// <summary>
        /// 音樂開關
        /// </summary>
        public bool MusicEnabled = true;
        /// <summary>
        /// 音效開關
        /// </summary>
        public bool SoundEffectEnabled = true;
    }
}
