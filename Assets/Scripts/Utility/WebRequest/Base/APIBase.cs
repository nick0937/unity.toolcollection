﻿using Cysharp.Threading.Tasks;
using System;
using System.Threading;
using UnityEngine;
using Utility.Tool;

namespace NickABC.Utils.WebRequest
{
    public class APIBase<TConfig, TArgs, TResponse> : IGameService, IInitializable, IConfigSetting
        where TConfig : WebRequestConfig, new()
        where TArgs : InitializeArgs
        where TResponse : BaseResponseData, new()
    {
        /// <summary>
        /// 初始化配置
        /// </summary>
        protected TConfig Config;
        /// <summary>
        /// 初始化參數
        /// </summary>
        protected TArgs Args;
        /// <summary>
        /// 回覆參數
        /// </summary>
        protected TResponse Response;
        public bool IsInitialize { get; protected set; }
        public event Action<TResponse> OnAPIComplete;
        public event EventHandler<TResponse> OnAPICompleteHandler;

        public event Action OnAPIFailed;
        public event EventHandler OnAPIFailedHandler;

        public event Action<float> OnAPIProgress;

        protected TaskWebRequest webRequest;
        protected CancellationTokenSource cts = new CancellationTokenSource();

        private object sender;
        public virtual void Initialize(InitializeArgs args)
        {
            Args = (TArgs)args;
            Config = new TConfig();
            Response = new TResponse();
            webRequest = new TaskWebRequest(Config);
            IsInitialize = true;
        }
        /// <summary>
        /// 執行API UniTask-Get
        /// </summary>
        /// <param name="url">連線位置</param>
        public virtual async UniTask CallRequestUniTask(string url)
        {
            Debugs.StateLog(this.ToString(), "CallAPI-Get", Debugs.Color.cyan);
            IProgress<float> progress = Progress.Create<float>((p) => { OnAPIProgress?.Invoke(p); });

            var result = await webRequest.Get<TResponse>(url, cts.Token, progress);

            if (result != null)
            {
                OnComplete(result);
            }
            else
            {
                OnFailure();
            }
        }
        /// <summary>
        /// 執行API-Get
        /// </summary>
        /// <param name="url">連線位置</param>
        public virtual async void CallRequest(string url)
        {
            await CallRequestUniTask(url);
        }
        /// <summary>
        /// 執行API-Get
        /// </summary>
        /// <param name="url">連線位置</param>
        /// <param name="sender">發送端</param>
        public virtual async void CallRequest(string url, object sender)
        {
            this.sender = sender;
            await CallRequestUniTask(url);
        }
        /// <summary>
        /// 執行API UniTask-Post
        /// </summary>
        /// <param name="url">連線位置</param>
        /// <param name="requestData">請求資料</param>
        public virtual async UniTask CallRequestTask<TRquest>(string url, TRquest requestData, bool isPost = true)
            where TRquest : class, new()
        {
            Debugs.StateLog(this.ToString(), "CallAPI-Post", Debugs.Color.cyan);
            IProgress<float> progress = Progress.Create<float>((p) => { OnAPIProgress?.Invoke(p); });
            var result = await webRequest.Post<TResponse>(url, requestData, cts.Token, progress, isPost);
            if (result != null)
            {
                OnComplete(result);
            }
            else
            {
                OnFailure();
            }
        }
        /// <summary>
        /// 執行API-Post
        /// </summary>
        /// <param name="url">連線位置</param>
        /// <param name="requestData">請求資料</param>
        public virtual async void CallRequest<TRquest>(string url, TRquest requestData, bool isPost = true)
            where TRquest : class, new()
        {
            await CallRequestTask(url, requestData, isPost);
        }
        /// <summary>
        /// 執行API-Post
        /// </summary>
        /// <param name="url">連線位置</param>
        /// <param name="requestData">請求資料</param>
        /// <param name="sender">發送端</param>
        public virtual async void CallRequest<TRquest>(string url, TRquest requestData, object sender, bool isPost = true)
            where TRquest : class, new()
        {
            this.sender = sender;
            await CallRequestTask(url, requestData, isPost);
        }
        public virtual async void CallRequestUpload(string url, WWWForm form)
        {
            Debugs.StateLog(this.ToString(), "CallAPI-PostUpload", Debugs.Color.cyan);
            IProgress<float> progress = Progress.Create<float>((p) => { OnAPIProgress?.Invoke(p); });
            var result = await webRequest.PostUploadFile<TResponse>(url, form, cts.Token, progress);
            if (result != null)
            {
                OnComplete(result);
            }
            else
            {
                OnFailure();
            }
        }
        /// <summary>
        /// 設定主機位置
        /// </summary>
        /// <param name="host">主機位置</param>
        public virtual void SetAPIHostName(string host)
        {
            Config.APIHostName = host;
        }
        /// <summary>
        /// 設定平台token
        /// </summary>
        /// <param name="token">平台token</param>
        public virtual void SetPlatformToken(string token)
        {
            Config.Token = token;
        }
        /// <summary>
        /// API 重置
        /// </summary>
        public virtual void Reset()
        {
            cts.Dispose();
            cts = new CancellationTokenSource();
        }
        #region Protected Method
        /// <summary>
        /// API 完成
        /// </summary>
        protected virtual void OnComplete(TResponse data)
        {
            Debugs.StateLog(ToString(), "OnComplete", Debugs.Color.cyan);
            OnAPIComplete?.Invoke(data);
            if (sender == null) sender = this;
            OnAPICompleteHandler?.Invoke(sender, data);
        }
        /// <summary>
        /// API 失敗
        /// </summary>
        protected virtual void OnFailure()
        {
            Debugs.StateLog(this.ToString(), "OnFailure", Debugs.Color.cyan);
            OnAPIFailed?.Invoke();
            if (sender == null) sender = this;
            OnAPIFailedHandler?.Invoke(sender, new EventArgs());
        }
        #endregion
    }
}