﻿using Cysharp.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NickABC.Utils.WebRequest;
using System;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.Networking;

public class TaskWebRequest
{

    private readonly WebRequestConfig webRequestConfig;
    public TaskWebRequest(WebRequestConfig config)
    {
        webRequestConfig = config;
    }
    /// <summary>
    /// UnityWebRequest-Get
    /// </summary>
    /// <param name="url">請求網址</param>
    /// <param name="cancelToken">取消任務</param>
    /// <param name="progress">回傳處理進度</param>
    public virtual async UniTask<TResponse> Get<TResponse>(string url, CancellationToken cancelToken = default, IProgress<float> progress = null)
        where TResponse : BaseResponseData, new()
    {
        try
        {
            using UnityWebRequest webRequest = UnityWebRequest.Get(url);

            SendLog(url);

            webRequest.SetRequestHeader("Content-Type", webRequestConfig.ContentType);
            webRequest.SetRequestHeader("Authorization", $"{webRequestConfig.Xtoken}{webRequestConfig.Token}");

            var awaiter = webRequest.SendWebRequest().ToUniTask(progress: progress, cancellationToken: cancelToken).GetAwaiter();

            await UniTask.WaitWhile(() => awaiter.IsCompleted == false);

            TResponse result = new TResponse();

            if (cancelToken.IsCancellationRequested) return null;

            if (webRequest.result == UnityWebRequest.Result.Success)
            {
                result = webRequestConfig.Deserialize<TResponse>(webRequest.downloadHandler.text);
                result.message = ResponseCode(url, result, webRequest.downloadHandler.text);
            }
            else
            {
                result.code = webRequest.responseCode;
                result.message = webRequest.error;
                ResponseErrorCode(webRequest.responseCode, webRequest.error);
            }
            result.origin = webRequest.downloadHandler.text;
            return result;
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            return default;
        }
    }
    /// <summary>
    /// UnityWebRequest-Get
    /// </summary>
    /// <param name="url">請求網址</param>
    /// <param name="json">傳遞資料</param>
    /// <param name="cancelToken">取消任務</param>
    /// <param name="progress">回傳處理進度</param>
    public virtual async UniTask<TResponse> Post<TResponse>(string url, string json, CancellationToken cancelToken = default, IProgress<float> progress = null, bool isPost = true)
        where TResponse : BaseResponseData, new()
    {
        try
        {
            byte[] bytePostData = Encoding.UTF8.GetBytes(json);

            using UnityWebRequest webRequest = UnityWebRequest.Put(url, bytePostData);

            SendLog(url, json);
            if (isPost)
                webRequest.method = "POST";

            webRequest.SetRequestHeader("Content-Type", webRequestConfig.ContentType);

            if (webRequestConfig.Token == null)
            {
                webRequest.SetRequestHeader("Authorization", webRequestConfig.Xtoken + webRequestConfig.Token);
            }

            var awaiter = webRequest.SendWebRequest().ToUniTask(progress: progress, cancellationToken: cancelToken).GetAwaiter();

            await UniTask.WaitWhile(() => awaiter.IsCompleted == false);

            TResponse result = new TResponse();

            if (webRequest.result == UnityWebRequest.Result.Success)
            {
                result = webRequestConfig.Deserialize<TResponse>(webRequest.downloadHandler.text);
                result.message = ResponseCode(url, result, webRequest.downloadHandler.text);
            }
            else
            {
                result.code = webRequest.responseCode;
                result.message = webRequest.error;
                ResponseErrorCode(webRequest.responseCode, webRequest.error);
            }

            result.origin = webRequest.downloadHandler.text;
            return result;
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            return default;
        }

    }
    public virtual async UniTask<TResponse> Post<TResponse>(string url, object data, CancellationToken cancelToken = default, IProgress<float> progress = null, bool isPost = true)
        where TResponse : BaseResponseData, new()
    {
        var json = webRequestConfig.Serialize(data);
        return await Post<TResponse>(url, json, cancelToken, progress, isPost);
    }
    public virtual async UniTask<TResponse> Post<TResponse>(string url, JObject json, CancellationToken cancelToken = default, IProgress<float> progress = null, bool isPost = true)
        where TResponse : BaseResponseData, new()
    {
        return await Post<TResponse>(url, json.ToString(), cancelToken, progress, isPost);
    }
    /// <summary>
    /// UnityWebRequest-Get
    /// </summary>
    /// <param name="url">請求網址</param>
    /// <param name="json">傳遞資料</param>
    /// <param name="cancelToken">取消任務</param>
    /// <param name="progress">回傳處理進度</param>
    public virtual async UniTask<TResponse> PostUploadFile<TResponse>(string url, WWWForm form, CancellationToken cancelToken = default, IProgress<float> progress = null, bool isPost = true)
        where TResponse : BaseResponseData, new()
    {
        try
        {
            using UnityWebRequest webRequest = UnityWebRequest.Post(url, form);

            if (webRequestConfig.Token == null)
            {
                webRequest.SetRequestHeader("Authorization", webRequestConfig.Xtoken + webRequestConfig.Token);
            }

            var awaiter = webRequest.SendWebRequest().ToUniTask(progress: progress, cancellationToken: cancelToken).GetAwaiter();

            await UniTask.WaitWhile(() => awaiter.IsCompleted == false);

            TResponse result = new TResponse();

            if (webRequest.result == UnityWebRequest.Result.Success)
            {
                result = webRequestConfig.Deserialize<TResponse>(webRequest.downloadHandler.text);
                result.message = ResponseCode(url, result, webRequest.downloadHandler.text);
            }
            else
            {
                result.code = webRequest.responseCode;
                result.message = webRequest.error;
                ResponseErrorCode(webRequest.responseCode, webRequest.error);
            }

            result.origin = webRequest.downloadHandler.text;
            return result;
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            return default;
        }

    }
    public virtual void SendLog(string url, string json = null)
    {
        Debug.Log($"C2P:: --> URL: {url} \n Jsondata:{json}");
    }
    protected string ResponseCode<TResponse>(string url, TResponse data, string json, string name = default)
        where TResponse : BaseResponseData, new()
    {
        Debug.Log($"P2C:: --> URL: {url} \n Jsondata:{json}");
        var log = data.code switch
        {
            200 => default,
            _ => $"{name}其他錯誤"
        };
        return log;
    }
    protected virtual string ResponseErrorCode(long code, string name)
    {
        Debugs.StateLog($"P2C --> API錯誤 : {name} ", $" ResponseCode : {code} ", Debugs.Color.cyan);
        string err = code switch
        {
            _ => $"{code.ToString()}參數錯誤"
        };
        return err;
    }
}