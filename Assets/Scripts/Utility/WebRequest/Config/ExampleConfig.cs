namespace NickABC.Utils.WebRequest
{
    public class ExampleConfig : WebRequestConfig
    {
        public ExampleConfig()
        {
            APIPathName = Path.PathName;
        }
        protected class Path
        {
            /// <summary>
            /// API路徑
            /// </summary>
            public const string PathName = "/Example";
        }
    }
}
