using System.ComponentModel;
using Utility.Extensions;
using Utility.Tool;

namespace NickABC.Utils.WebRequest
{
    public class WebRequestConfig : JsonSerializationOption, IWebRequestOption
    {
        public string Http { get => "http://"; }

        public string Https { get => "https://"; }

        public string URL { get => $"{Https}{APIHostName}{APIBasePath}{APIPathName}"; }

        public string APIHostName { get => apiHost; set { apiHost = value; } }

        private string apiHost;

        public BasePath APIType { get { return apiType; } set { apiType = value; } }

        private BasePath apiType = default(BasePath);

        public string APIBasePath { get { return apiType.GetDescription(); } }

        public string APIPathName { get => apiPath; set => apiPath = value; }

        private string apiPath;
        public string ContentType { get => contentType; set => contentType = value; }

        private string contentType = "application/json";

        public string Xtoken { get => xtoken; set => xtoken = value; }

        private string xtoken = "Bearer ";

        public string Token { get => token; set => token = value; }

        private string token;

        public int Timeout { get => timeout; set => timeout = value; }

        private int timeout = 20;

        public enum BasePath
        {
            [Description("/api")]
            api = 1,
            [Description("/api2")]
            api2 = 0
        }
    }
}
