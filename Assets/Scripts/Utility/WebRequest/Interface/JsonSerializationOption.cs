﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Utility.Tool
{
    public class JsonSerializationOption : ISerializationOption
    {
        public virtual string Serialize<T>(T data)
        {
            try
            {
                var result = JsonUtility.ToJson(data);
                return result;
            }
            catch (Exception ex)
            {
                Debug.LogError($"Could not parse response {data}. /n {ex.Message}");
                return default;
            }
        }
        public virtual T Deserialize<T>(string text)
        {
            try
            {
                var result = JsonUtility.FromJson<T>(text);
                return result;
            }
            catch (Exception ex)
            {
                Debug.LogError($"Could not parse response {text}. /n {ex.Message}");
                return default;
            }
        }
        public virtual string SerializeList<T>(List<T> data)
        {
            try
            {
                var result = JsonUtility.ToJson(new Serialization<T>(data));
                return result;
            }
            catch (Exception ex)
            {
                Debug.LogError($"Could not parse response {data}. /n {ex.Message}");
                return default;
            }
        }
        public virtual List<T> DeserializeList<T>(string text)
        {
            try
            {
                var result = JsonUtility.FromJson<Serialization<T>>(text).ToList();
                return result;
            }
            catch (Exception ex)
            {
                Debug.LogError($"Could not parse response {text}. /n {ex.Message}");
                return default;
            }
        }
    }
}