﻿namespace Utility.Tool
{
    public interface IConfigSetting
    {
        void SetAPIHostName(string host);
        void SetPlatformToken(string token);
    }
}