﻿namespace Utility.Tool
{
    public interface IWebRequestOption
    {
        /// <summary>
        /// HTTP傳輸協定
        /// </summary>
        string Http { get; }
        /// <summary>
        /// HTTPS傳輸協定
        /// </summary>
        string Https { get; }
        /// <summary>
        /// 主機位置
        /// </summary>
        string APIHostName { get; }
        /// <summary>
        /// APIBase路徑
        /// </summary>
        string APIBasePath { get; }
        /// <summary>
        /// API路徑
        /// </summary>
        string APIPathName { get; }
        /// <summary>
        /// 內容類型
        /// </summary>
        string ContentType { get; }
        /// <summary>
        /// Token標頭
        /// </summary>
        string Xtoken { get; }
        /// <summary>
        /// Token
        /// </summary>
        string Token { get; }
        /// <summary>
        /// 連線逾時時間(秒)
        /// </summary>
        int Timeout { get; }
    }
}