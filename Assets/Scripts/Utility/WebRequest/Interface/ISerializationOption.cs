﻿namespace Utility.Tool
{
    public interface ISerializationOption
    {
        string Serialize<T>(T obj);
        T Deserialize<T>(string text);
    }
}