/*
 *  Project Name：Utility
 *
 *  Programmer：Nick Tseng
 *
 *  Start Date：2022/07/29
 *  
 *  UnityVersion：2020.3.15f2
 */

namespace Utility.Tool
{
    public interface IWebRequestGet
    {
        /// <summary>
        /// 呼叫API
        /// </summary>
        void CallAPI();
    }
    public interface IWebRequestPost<T>
    {
        /// <summary>
        /// 呼叫API
        /// </summary>
        /// <param name="data">請求參數</param>
        void CallAPI(T data);
    }
}