namespace NickABC.Utils.WebRequest
{
    /// <summary>
    /// API
    /// </summary>
    public class ExampleAPI : APIBase<ExampleConfig, PipArgs, ExampleResponseData>
    {
        #region Public Method
        public override void Initialize(InitializeArgs args)
        {
            base.Initialize(args);
        }
        /// <summary>
        /// 執行ExampleAPI
        /// </summary>
        /// <param name="success">執行成功</param>
        /// <param name="failure">執行失敗</param>
        public void DoExampleAPI()
        {
            UnityEngine.Assertions.Assert.IsTrue(IsInitialize, $"PipAPI has not been initialized");
            CallRequest(Config.URL);
        }
        #endregion
        #region Protected Method
        /// <summary>
        /// ExampleAPI完成
        /// </summary>
        protected override void OnComplete(ExampleResponseData data)
        {
            base.OnComplete(data);
        }
        /// <summary>
        /// ExampleAPI失敗
        /// </summary>
        protected override void OnFailure()
        {
            base.OnFailure();
        }
        #endregion
    }
}
