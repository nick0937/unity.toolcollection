using System;
using System.Collections.Generic;
namespace NickABC.Utils.WebRequest
{
    [Serializable]
    public class ExampleResponseData : BaseResponseData
    {
        public int code;
        public List<Data> data;
        [Serializable]
        public class Data
        {
            public int DataInt;
            public string DataStr;
        }
    }
}
