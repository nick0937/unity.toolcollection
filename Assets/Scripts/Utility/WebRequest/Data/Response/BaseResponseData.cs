/*
 *  Project Name：philippines
 *
 *  Programmer：Nick Tseng
 *
 *  Start Date：2022/08/02
 *  
 *  UnityVersion：2020.3.15f2
 */

using System;

namespace NickABC.Utils.WebRequest
{
    [Serializable]
    public class BaseResponseData : EventArgs
    {
        /// <summary>
        /// 狀態碼 
        /// </summary>
        public long code;
        /// <summary>
        /// 錯誤訊息 
        /// </summary>
        public string message;
        /// <summary>
        /// 原始Json
        /// </summary>
        public string origin;
    }
}