/*
 *  Description : thailand
 *
 *  Author ： Nick Tseng
 *
 *  Date ： 2023/02/24
 */

namespace Utility
{
    using System;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.Localization;
    using UnityEngine.Localization.Components;
    [Serializable]
    public class UnityEventFont : UnityEvent<Font>
    {
    }

    [AddComponentMenu("Localization/Asset/Localize Font Event")]
    public class LocalizeFontEvent : LocalizedAssetEvent<Font, LocalizedFont, UnityEventFont>
    {
        #region Variable

        #endregion
        #region Public Method

        #endregion
        #region Protected Method

        #endregion
        #region Private Method

        #endregion
    }
}