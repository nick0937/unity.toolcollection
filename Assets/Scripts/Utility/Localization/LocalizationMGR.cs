/*
 *  Project Name：Utility
 *
 *  Programmer：Nick　Tseng
 *
 *  Create Date：2022/10/13
 *  
 *  UnityVersion：2020.3.15f2
 */

using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Localization.Settings;
using UnityEngine.Localization.Tables;
using UnityEngine.ResourceManagement.AsyncOperations;
using Utility.Singleton;

namespace Utility.Localization
{
    public class LocalizationMGR : MonoSingleton<LocalizationMGR>
    {
        #region Variable
        /// <summary>
        /// 是否初始化完成
        /// </summary>
        public bool InitializeCompleted => _isInitialized && _currentStringTable != null && _currentAssetTable != null;
        /// <summary>
        /// 是否 StringTable 準備完成
        /// </summary>
        public bool IsStringTableReady => _currentStringTable != null;
        /// <summary>
        /// 是否 AssetTable 準備完成
        /// </summary>
        public bool IsAssetTableReady => _currentAssetTable != null;
        /// <summary>
        /// 是否本地化初始完成
        /// </summary>
        public bool IsInitialized => _isInitialized;
        /// <summary>
        /// 語系轉換完成
        /// </summary>
        public bool ChangeCompleted { get { return !_isChanging; } }
        /// <summary>
        /// 是否本地化初始完成
        /// </summary>
        private bool _isInitialized;
        /// <summary>
        /// 目前使用的 StringTable 名稱
        /// </summary>
        private string _currentStringTableName = "Localization_String";
        /// <summary>
        /// 目前使用的 AssetTable 名稱
        /// </summary>
        private string _currentAssetTableName = "Localization_Asset";
        /// <summary>
        /// 目前使用的 StringTable
        /// </summary>
        private StringTable _currentStringTable = null;
        /// <summary>
        /// 目前使用的 AssetTable
        /// </summary>
        private AssetTable _currentAssetTable = null;
        /// <summary>
        /// 是否語系轉換中
        /// </summary>
        private bool _isChanging;
        #endregion
        #region Public Method
        /// <summary>
        /// 設定目前使用 StringTable
        /// </summary>
        /// <param name="name">StringTable 名稱</param>
        public void SetStringTable(string name)
        {
            if (_isChanging || !_isInitialized)
            {
                Debug.LogError($"LocalizationMGR has not been initialized, Please call the Initialize method first and confirm that the InitializeCompleted variable is true.");
                return;
            }
            if (_currentStringTableName == name) return;
            _currentStringTableName = name;
            ChangeStringTable();
        }
        /// <summary>
        /// 設定目前使用 AssetTable
        /// </summary>
        /// <param name="name">AssetTable 名稱</param>
        public void SetAssetTable(string name)
        {
            if (_isChanging || !_isInitialized)
            {
                Debug.LogError($"LocalizationMGR has not been initialized, Please call the Initialize method first and confirm that the InitializeCompleted variable is true.");
                return;
            }
            if (_currentAssetTableName == name) return;
            _currentAssetTableName = name;
            ChangeAssetTable();
        }
        /// <summary>
        /// 設定目前使用語系
        /// </summary>
        /// <param name="localeID">語系排序</param>
        public void SetLanguage(int localeID)
        {
            if (_isChanging || !_isInitialized)
            {
                Debug.LogError($"LocalizationMGR has not been initialized, Please call the Initialize method first and confirm that the InitializeCompleted variable is true.");
                return;
            }
            if (LocalizationSettings.SelectedLocale == LocalizationSettings.AvailableLocales.Locales[localeID]) return;
            LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[localeID];
        }
        /// <summary>
        /// 取得本地化文字
        /// </summary>
        /// <param name="key">Table Key</param>
        /// <returns>本地化文字</returns>
        public string GetLocalizedString(string key)
        {
            return GetLocalizedString(key, _currentStringTable);
        }
        /// <summary>
        /// 取得本地化文字
        /// </summary>
        /// <param name="key">Table Key</param>
        /// <param name="table">StringTable</param>
        /// <returns>本地化文字</returns>
        public string GetLocalizedString(string key, StringTable table)
        {
            if (InitializeCompleted)
            {
                var entry = table.GetEntry(key);
                return entry.IsSmart? entry?.LocalizedValue : entry?.GetLocalizedString();
            }
            else
            {
                Debug.LogWarning($"LocalizationMGR has not been initialized, Please call the Initialize method first and confirm that the InitializeCompleted variable is true.");
                return default;
            }
        }
        /// <summary>
        /// 取得本地化檔案
        /// </summary>
        /// <typeparam name="TObject">檔案物件類型</typeparam>
        /// <param name="key">Table Key</param>
        /// <returns>檔案物件</returns>
        public TObject GetLocalizedAsset<TObject>(string key) where TObject : Object
        {
            return GetLocalizedAsset<TObject>(key, _currentAssetTable);
        }
        /// <summary>
        /// 取得本地化檔案
        /// </summary>
        /// <typeparam name="TObject">檔案物件類型</typeparam>
        /// <param name="key">Table Key</param>
        /// <param name="table">AssetTable</param>
        /// <returns>檔案物件</returns>
        public TObject GetLocalizedAsset<TObject>(string key, AssetTable table) where TObject : Object
        {
            return table.GetAssetAsync<TObject>(key).WaitForCompletion();
        }
        /// <summary>
        /// 取得 StringTable
        /// </summary>
        /// <param name="name">Table Name</param>
        /// <returns>StringTable</returns>
        public StringTable GetStringTable(string name)
        {
            var tableOp = LocalizationSettings.StringDatabase.GetTableAsync(name);
            return tableOp.WaitForCompletion();
        }
        /// <summary>
        /// 取得 AssetTable
        /// </summary>
        /// <param name="name">Table Name</param>
        /// <returns>AssetTable</returns>
        public AssetTable GetAssetTable(string key)
        {
            var tableOp = LocalizationSettings.AssetDatabase.GetTableAsync(key);
            return tableOp.WaitForCompletion();
        }
        /// <summary>
        /// 初始化
        /// </summary>
        public async void Initialize()
        {
            await LocalizationSettings.InitializationOperation;
            LocalizationSettings.AvailableLocales.Locales.Sort();
            LocalizationSettings.StringDatabase.GetTableAsync(_currentStringTableName).Completed += OnLoadStringTableCompleted;
            LocalizationSettings.AssetDatabase.GetTableAsync(_currentAssetTableName).Completed += OnLoadAssetTableCompleted;
            LocalizationSettings.SelectedLocaleChanged += locale => ChangeStringTable();
            LocalizationSettings.SelectedLocaleChanged += locale => ChangeAssetTable();
            _isInitialized = true;
        }
        #endregion
        #region Protected Method
        protected override string GetSingletonName()
        {
            return "LocalizationMGR";
        }
        protected override bool IsDestroyOnLoad()
        {
            return false;
        }
        #endregion
        #region Private Method
        /// <summary>
        /// 載入 StringTable 完成
        /// </summary>
        /// <param name="obj"></param>
        private void OnLoadStringTableCompleted(AsyncOperationHandle<StringTable> obj)
        {
            if (obj.Status == AsyncOperationStatus.Succeeded)
            {
                if (obj.Result != null)
                {
                    _currentStringTable = obj.Result;
                }
            }
        }
        /// <summary>
        /// 載入 AssetTable 完成
        /// </summary>
        /// <param name="obj"></param>
        private void OnLoadAssetTableCompleted(AsyncOperationHandle<AssetTable> obj)
        {
            if (obj.Status == AsyncOperationStatus.Succeeded)
            {
                if (obj.Result != null)
                {
                    _currentAssetTable = obj.Result;
                }
            }
        }
        /// <summary>
        /// 更改 StringTable
        /// </summary>
        private async void ChangeStringTable()
        {
            _isChanging = true;

            var tableOp = LocalizationSettings.StringDatabase.GetTableAsync(_currentStringTableName);

            tableOp.Completed += OnLoadStringTableCompleted;

            await tableOp;

            _isChanging = false;
        }
        /// <summary>
        /// 更改 AssetTable
        /// </summary>
        private async void ChangeAssetTable()
        {
            _isChanging = true;

            var tableOp = LocalizationSettings.AssetDatabase.GetTableAsync(_currentAssetTableName);

            tableOp.Completed += OnLoadAssetTableCompleted;

            await tableOp;

            _isChanging = false;
        }
        #endregion

    }
}