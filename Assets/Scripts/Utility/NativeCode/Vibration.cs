/*
 *  Description : Utility
 *
 *  Author ： Nick Tseng
 *
 *  Date ： 2023/10/23
 */

using UnityEngine;

namespace Utility
{
    public static class Vibration
    {
        #region Variable
        /// <summary>
        /// 震動是否啟用
        /// </summary>
        private static bool _isEnabled;
        #endregion
        #region Unity Method

        #endregion
        #region Public Method
        /// <summary>
        /// 設定震動啟用
        /// </summary>
        /// <param name="isEnabled">設定啟用</param>
        public static void SetEnabled(bool isEnabled)
        {
            _isEnabled = isEnabled;
        }
        /// <summary>
        /// 震動開啟狀態
        /// </summary>
        /// <returns>震動狀態</returns>
        public static bool GetEnabled()
        {
            return _isEnabled;
        }
        /// <summary>
        /// 震動
        /// </summary>
        public static void Vibrate()
        {
            if (!_isEnabled)
            {
                Debug.LogWarning("Vibration is not authorized, please set enabled and try again.!");
                return;
            }
            Handheld.Vibrate();
        }
        #endregion
        #region Protected Method

        #endregion
        #region Private Method

        #endregion
    }
}