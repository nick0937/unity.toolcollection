/*
 *  Project Name：philippines
 *
 *  Programmer：Nick Tseng
 *
 *  Start Date：2022/08/26
 *  
 *  UnityVersion：2020.3.15f2
 */

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
    [AddComponentMenu("UI/Toggle Group EX", 32)]
    [DisallowMultipleComponent]
    public class ToggleGroupEX : UIBehaviour
    {
        #region Variable
        [SerializeField] private bool m_AllowSwitchOff = false;

        public bool allowSwitchOff { get { return m_AllowSwitchOff; } set { m_AllowSwitchOff = value; } }

        protected List<ToggleEX> m_Toggles = new List<ToggleEX>();
        #endregion

        #region Public Method
        public void NotifyToggleOn(ToggleEX toggle, bool sendCallback = true)
        {
            ValidateToggleIsInGroup(toggle);

            for (var i = 0; i < m_Toggles.Count; i++)
            {
                if (m_Toggles[i] == toggle)
                    continue;

                if (sendCallback)
                    m_Toggles[i].isOn = false;
                else
                    m_Toggles[i].SetIsOnWithoutNotify(false);
            }
        }

        public void UnregisterToggle(ToggleEX toggle)
        {
            if (m_Toggles.Contains(toggle))
                m_Toggles.Remove(toggle);
        }

        public void RegisterToggle(ToggleEX toggle)
        {
            if (!m_Toggles.Contains(toggle))
                m_Toggles.Add(toggle);
        }
        public void EnsureValidState()
        {
            if (!allowSwitchOff && !AnyTogglesOn() && m_Toggles.Count != 0)
            {
                m_Toggles[0].isOn = true;
                NotifyToggleOn(m_Toggles[0]);
            }

            IEnumerable<ToggleEX> activeToggles = ActiveToggles();

            if (activeToggles.Count() > 1)
            {
                ToggleEX firstActive = GetFirstActiveToggle();

                foreach (ToggleEX toggle in activeToggles)
                {
                    if (toggle == firstActive)
                    {
                        continue;
                    }
                    toggle.isOn = false;
                }
            }
        }
        public bool AnyTogglesOn()
        {
            return m_Toggles.Find(x => x.isOn) != null;
        }

        public IEnumerable<ToggleEX> ActiveToggles()
        {
            return m_Toggles.Where(x => x.isOn);
        }
        public ToggleEX GetFirstActiveToggle()
        {
            IEnumerable<ToggleEX> activeToggles = ActiveToggles();
            return activeToggles.Count() > 0 ? activeToggles.First() : null;
        }
        public void SetAllTogglesOff(bool sendCallback = true)
        {
            bool oldAllowSwitchOff = m_AllowSwitchOff;
            m_AllowSwitchOff = true;

            if (sendCallback)
            {
                for (var i = 0; i < m_Toggles.Count; i++)
                    m_Toggles[i].isOn = false;
            }
            else
            {
                for (var i = 0; i < m_Toggles.Count; i++)
                    m_Toggles[i].SetIsOnWithoutNotify(false);
            }

            m_AllowSwitchOff = oldAllowSwitchOff;
        }
        #endregion
        #region Protected Method
        protected ToggleGroupEX()
        { }
        protected override void Start()
        {
            EnsureValidState();
            base.Start();
        }

        protected override void OnEnable()
        {
            EnsureValidState();
            base.OnEnable();
        }
        #endregion
        #region Private Method
        private void ValidateToggleIsInGroup(ToggleEX toggle)
        {
            if (toggle == null || !m_Toggles.Contains(toggle))
                throw new ArgumentException(string.Format("Toggle {0} is not part of ToggleGroupEX {1}", new object[] { toggle, this }));
        }
        #endregion
    }
}