﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
namespace NickABC.Utils.UI
{
    /// <summary>
    /// 給Button、Toggle或者Image、Text元件使用，可以偵測點擊相關事件(有開啟Raycast Target)
    /// 掛載此腳本的GameObject，所有子物件皆被偵測點擊(Image、Text或其他UI，Button、Toggle除外，需要在這兩者上掛此腳本才會有效)
    /// </summary>
    public class ClickEvent : MonoBehaviour, IPointerClickHandler
    {
        void Awake()
        {
            Component[] allComponents = GetComponents<Component>();
            for (int i = 0; i < allComponents.Length; i++)
            {
                if (allComponents[i].GetType().ToString() == this.GetType().ToString())
                {
                    if (allComponents[i].GetHashCode() != this.GetHashCode())
                    {
                        Debug.LogError("Gameobject：" + name + "上的腳本：" + allComponents[i].GetType().ToString() + "重複Add，已將重複Add的刪除");
                        Destroy(allComponents[i]);
                    }
                }
            }
        }
        #region Public Method
        #region 設定Call Back 方式1
        /// <summary>
        /// 直接設定按鈕被點擊的對應呼叫,不帶回叫參數
        /// </summary>
        public Action OnClickCall = null;
        #endregion

        #region 設定Call Back 方式2
        Action<object> onClickCallWithCustomObj = null;
        object customObject = null;
        /// <summary>
        /// 設定按鈕點擊後，Call Back要回傳的物件型態與物件實體
        /// </summary>
        /// <typeparam name="T">設定點擊發生時，需要回吐給你的物件type</typeparam>
        /// <param name="customObj">設定點擊發生時，需要回吐給你的物件Instance</param>
        public void OnClickCallWithCustomObj<T>(T customObj, Action<T> clickCallWithCustomObj)
        {
            customObject = customObj;
            onClickCallWithCustomObj = ConvertActionToActionObj<T>(clickCallWithCustomObj);
        }
        #endregion

        #region 設定Call Back 方式3
        Action<bool, object> onClickCallWithStatusCustomObj = null;
        object statusCustomObj = null;

        /// <summary>
        /// 設定按鈕點擊後，Call Back要回傳的物件型態與物件實體
        /// </summary>
        /// <typeparam name="T">設定點擊發生時，需要回吐給你的物件type</typeparam>
        /// <param name="customObj">設定點擊發生時，需要回吐給你的物件Instance</param>
        /// <param name="clickCallWithCustomObj">Action中的Bool 代表按鈕狀態(toggle：選取(true)、未選取(false),btn：只有點下(true),Image：只有點下(true))</param>
        public void OnClickCallWithCustomObj<T>(T customObj, Action<bool, T> clickCallWithCustomObj)
        {
            statusCustomObj = customObj;
            onClickCallWithStatusCustomObj = ConvertActionToActionObjWithBool<T>(clickCallWithCustomObj);
        }
        #endregion

        #region 設定Call Back 方式4
        Action<bool> onClickCallWithStatus = null;

        /// <summary>
        /// 設定按鈕點擊後，Call Back要回傳的物件型態與物件實體
        /// </summary>
        /// <param name="clickCallWithBool">Action中的Bool 代表按鈕狀態(toggle：選取(true)、未選取(false),btn：只有點下(true),Image：只有點下(true))</param>
        public void OnClickCallWithBool(Action<bool> clickCallWithBool)
        {
            onClickCallWithStatus = ConvertActionToActionBool(clickCallWithBool);
        }
        #endregion

        public virtual void OnPointerClick(PointerEventData i_eventData)
        {
            Toggle toggle = GetComponent<Toggle>();
            Button button = GetComponent<Button>();

            int nullCount = 0;
            int callCount = 0;

            if (OnClickCall is null) nullCount++; else callCount++;
            if (onClickCallWithCustomObj is null) nullCount++; else callCount++;
            if (onClickCallWithStatusCustomObj is null) nullCount++; else callCount++;
            if (onClickCallWithStatus is null) nullCount++; else callCount++;

            if (nullCount >= 4)
            {
                Debug.LogError("Click時，沒有呼叫Function");
                return;
            }

            if (callCount > 1)
            {
                Debug.LogError("Click時，呼叫多個Function");
                return;
            }

            if (OnClickCall != null)
            {
                if (button == null)
                {//不是按鈕，就固定呼叫call back就可以
                    OnClickCall();
                }
                else
                {//是按鈕
                    if (button.interactable == true)
                    {
                        OnClickCall();
                    }
                    else
                    {//不呼叫
                    }
                }
            }

            if (onClickCallWithCustomObj != null)
            {
                if (button == null)
                {//不是按鈕，就固定回傳
                    onClickCallWithCustomObj(customObject);
                }
                else
                {//是按鈕
                    if (button.interactable == true)
                    {
                        onClickCallWithCustomObj(customObject);
                    }
                    else
                    {//不呼叫
                    }
                }
            }

            if (onClickCallWithStatusCustomObj != null)
            {
                if (toggle != null)
                {
                    //回傳toggle 按鈕的狀態
                    onClickCallWithStatusCustomObj(toggle.isOn, statusCustomObj);
                }
                else
                {
                    if (button == null)
                    {
                        //不是button ,固定回傳true
                        onClickCallWithStatusCustomObj(true, statusCustomObj);
                    }
                    else
                    {
                        //是button，回傳interactable
                        onClickCallWithStatusCustomObj(button.interactable, statusCustomObj);
                    }
                }
            }

            if (onClickCallWithStatus != null)
            {
                if (toggle != null)
                {
                    //回傳toggle 按鈕的狀態
                    onClickCallWithStatus(toggle.isOn);
                }
                else
                {
                    if (button == null)
                    {
                        //不是button ,固定回傳true
                        onClickCallWithStatus(true);
                    }
                    else
                    {
                        //是button，回傳interactable
                        onClickCallWithStatus(button.interactable);
                    }
                }
            }
        }
        #endregion
        #region Private Method
        private Action<object> ConvertActionToActionObj<T>(Action<T> i_ActionT)
        {
            if (i_ActionT == null)
                return null;
            else
                return new Action<object>(o => i_ActionT((T)o));
        }

        private Action<bool, object> ConvertActionToActionObjWithBool<T>(Action<bool, T> i_ActionT)
        {
            if (i_ActionT == null)
                return null;
            else
                return new Action<bool, object>((b, o) => i_ActionT(b, (T)o));
        }

        private Action<bool> ConvertActionToActionBool(Action<bool> i_ActionT)
        {
            if (i_ActionT == null)
                return null;
            else
                return new Action<bool>((b) => i_ActionT(b));
        }
        #endregion
    }
}
