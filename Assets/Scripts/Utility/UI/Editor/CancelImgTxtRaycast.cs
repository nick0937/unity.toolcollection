using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Utility.UI
{
    public class CancelImgTxtRaycast
    {
        [MenuItem("GameObject/UI/Ex/Image-NoRaycast", false, 1)]
        static void CreatImage(MenuCommand menuCommand)
        {
            EditorApplication.ExecuteMenuItem("GameObject/UI/Image");
            GameObject go = Selection.activeGameObject;
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            go.GetComponent<Image>().raycastTarget = false;
        }

        [MenuItem("GameObject/UI/Ex/Text-NoRaycast", false, 2)]
        static void CreatText(MenuCommand menuCommand)
        {
            EditorApplication.ExecuteMenuItem("GameObject/UI/Text");
            GameObject go = Selection.activeGameObject;
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            go.GetComponent<Text>().raycastTarget = false;
        }
    }
}