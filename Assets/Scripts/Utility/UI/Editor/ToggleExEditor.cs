/*
 *  Project Name：philippines
 *
 *  Programmer：Nick Tseng
 *
 *  Start Date：2022/08/29
 *  
 *  UnityVersion：2020.3.15f2
 */

using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Utility.UI
{
    public class ToggleExEditor
    {
        [MenuItem("GameObject/UI/Ex/ToggleEx", false, 3)]
        static void CreatToggle(MenuCommand menuCommand)
        {
            GameObject toggle = new GameObject("ToggleEX", typeof(ToggleEX));
            ToggleEX toggleExp = toggle.GetComponent<ToggleEX>();

            GameObject background = new GameObject("Background", typeof(Image));
            background.transform.SetParent(toggle.transform);

            toggleExp.targetGraphic = background.GetComponent<Image>();

            GameObject mark = new GameObject("Checkmark", typeof(Image));
            Sprite sprite = null;
            Object[] objs = AssetDatabase.LoadAllAssetsAtPath("Resources/unity_builtin_extra");
            for (int i = 0; i < objs.Length; i++)
            {
                if (objs[i].name == "Checkmark" && objs[i].GetType() == typeof(Sprite))
                {
                    sprite = (Sprite)objs[i];
                    break;
                }
            }
            Image markImage = mark.GetComponent<Image>();
            markImage.sprite = sprite;
            markImage.raycastTarget = false;
            toggleExp.graphic = markImage;
            mark.transform.SetParent(background.transform);

            if (Selection.activeGameObject == null)
            {
                Canvas canvas = Object.FindObjectOfType<Canvas>();

                if (canvas != null)
                {
                    toggle.GetComponent<RectTransform>().SetParent(canvas.transform, false);
                }
            }
            else
            {
                toggle.GetComponent<RectTransform>().SetParent(Selection.activeGameObject.transform, false);
            }

            Selection.activeGameObject = toggle;
        }
    }
}