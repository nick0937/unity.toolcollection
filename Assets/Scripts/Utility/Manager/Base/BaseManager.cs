﻿using System;
using UnityEngine;
namespace NickABC.Utils.Manager
{
    public class BaseManager : MonoBehaviour, IGameService, IInitializable
    {
        public bool IsInitialize { get; protected set; }
        public virtual void Initialize(InitializeArgs args)
        {
            try
            {
                RegisterService();
                CheckService();
                RegisterEvent();
                InitializeData();
                OnInitializeSuccess();
            }
            catch (Exception ex)
            {
                Debug.LogException(ex, this);
                OnInitializeFailed(ex.Message);
            }
        }
        void Awake()
        {
            OnAwake();
        }
        void Start()
        {
            OnStart();
        }
        #region Protected Method
        /// <summary>
        /// 註冊服務-CanvasController
        /// </summary>
        protected virtual void RegisterService()
        {
            Debugs.StateLog(name, "RegisterService", Debugs.Color.green);
        }
        /// <summary>
        /// 檢查服務
        /// </summary>
        protected virtual void CheckService()
        {
            Debugs.StateLog(name, "CheckService", Debugs.Color.green);
        }
        /// <summary>
        /// 註冊事件
        /// </summary>
        protected virtual void RegisterEvent()
        {
            Debugs.StateLog(name, "RegisterEvent", Debugs.Color.green);
        }
        /// <summary>
        /// 註銷事件
        /// </summary>
        protected virtual void UnregisterEvent()
        {
            Debugs.StateLog(name, "UnregisterEvent", Debugs.Color.green);
        }
        /// <summary>
        /// 初始化資料
        /// </summary>
        protected virtual void InitializeData()
        {
            Debugs.StateLog(name, "InitializeData", Debugs.Color.green);
        }
        /// <summary>
        /// 初始化成功
        /// </summary>
        protected virtual void OnInitializeSuccess()
        {
            Debugs.StateLog(name, "OnInitializeSuccess", Debugs.Color.green);
            IsInitialize = true;
        }
        /// <summary>
        /// 初始化失敗
        /// </summary>
        protected virtual void OnInitializeFailed(string msg)
        {
            Debugs.StateLog(name, "OnInitializeFailed", Debugs.Color.green);
            IsInitialize = false;
        }
        #region FlowChart
        protected virtual void OnAwake()
        {

        }
        protected virtual void OnStart()
        {

        }
        protected virtual void OnDestroy()
        {
            UnregisterEvent();
        }
        #endregion
        #region ActionEvent

        #endregion
        #endregion
    }
}