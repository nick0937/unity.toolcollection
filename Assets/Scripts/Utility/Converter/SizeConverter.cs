/*
 *  Project Name：Utility.Tool
 *
 *  Programmer：Nick　Tseng
 *
 *  Create Date：2022/10/03
 *  
 *  UnityVersion：2020.3.15f2
 */

using System;
using Utility.Singleton;

namespace Utility.Tool
{
    public class SizeConverter : Singleton<SizeConverter>
    {
        private readonly string[] suffixes =
    {
        "Bytes", "KB", "MB", "GB", "TB", "PB"
    };
        public string FormatSize(long bit)
        {
            int counter = 0;
            decimal number = bit;
            while (Math.Round(number / 1024) >= 1)
            {
                number = number / 1024;
                counter++;
            }
            return string.Format("{0:n1}{1}", number, suffixes[counter]);
        }
    }
}