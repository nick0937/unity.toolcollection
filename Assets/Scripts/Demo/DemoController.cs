﻿using NickABC.Utils.UI;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Utility.Extensions;
using Utility.Other;

public class DemoController : MonoBehaviour
{
    [Header("Singleton 介面")]
    /// <summary>
    /// 主要的Canvas UI
    /// </summary>
    public GameObject Cav_SingletonDemo;
    [Header("ClickEvent 介面")]
    /// <summary>
    /// 主要的Canvas UI
    /// </summary>
    public GameObject Cav_ClickEventDemo;
    /// <summary>
    /// 顯示物件按鈕
    /// </summary>
    private GameObject btnShow;
    /// <summary>
    /// 隱藏物件按鈕
    /// </summary>
    private GameObject btnHide;
    /// <summary>
    /// 刪除物件按鈕
    /// </summary>
    private GameObject btnDestroy;
    /// <summary>
    /// 點擊事件測試按鈕
    /// </summary>
    private GameObject btnClickEvent01;
    /// <summary>
    /// 點擊事件測試按鈕
    /// </summary>
    private GameObject btnClickEvent02;
    /// <summary>
    /// 儲存搜尋到的Obj
    /// </summary>
    private Hashtable rootTransformHt = new Hashtable();

    void Awake()
    {
        btnShow = Utilities.Select(rootTransformHt, Cav_SingletonDemo.transform, "btn_Show");
        btnHide = Utilities.Select(rootTransformHt, Cav_SingletonDemo.transform, "btn_Hide");
        btnDestroy = Utilities.Select(rootTransformHt, Cav_SingletonDemo.transform, "btn_Destroy");
        btnClickEvent01 = Utilities.Select(rootTransformHt, Cav_ClickEventDemo.transform, "btn_ClickEvent01");
        btnClickEvent02 = Utilities.Select(rootTransformHt, Cav_ClickEventDemo.transform, "btn_ClickEvent02");

        btnShow.GetOrAddComponent<ClickEvent>().OnClickCall = SingletonShow;
        btnHide.GetOrAddComponent<ClickEvent>().OnClickCall = SingletonHide;
        btnDestroy.GetOrAddComponent<ClickEvent>().OnClickCall = SingletonDestroy;
        btnClickEvent01.GetOrAddComponent<ClickEvent>().OnClickCallWithCustomObj(btnClickEvent01, DoClickEventCallWithCustom);
        btnClickEvent02.GetOrAddComponent<ClickEvent>().OnClickCallWithCustomObj(btnClickEvent02, DoClickEventCallWithStatusCustom);

    }
    void SingletonShow()
    {
        SingletonCube.Inst.Show();
    }
    void SingletonHide()
    {
        SingletonCube.Inst.Hide();
    }
    void SingletonDestroy()
    {
        SingletonCube.Inst.Destroy();
    }
    void DoClickEventCallWithCustom(GameObject target)
    {
        Debug.Log(target.name);
    }
    void DoClickEventCallWithStatusCustom(bool isOn, GameObject target)
    {
        Debug.LogFormat("{0} : {1}{2}", target.name, "Before Interactable is ", target.GetComponent<Button>().interactable);
        target.GetComponent<Button>().interactable = !isOn;
        Debug.LogFormat("{0} : {1}{2}", target.name, "After Interactable is ", target.GetComponent<Button>().interactable);
    }
}
