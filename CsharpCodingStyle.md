# CsharpCodingStyle

Unity專案程式風格

## 範例
```csharp
/*
 * Description:This is a example class
 * Author: XXXX
 * Date: 20221124
 */

using System;

namespace Namespace
{
    public class Class
    {
        public int Property { get; set; }

        public delegate void DoSomethingEventHandler();

        public event EvnetHandler OnEvent;

        public event Action OnAction;

        public readonly int Field1;

        protected int Field2;

        private int _field3;

        public void Method(int args)
        {
            //局部變量(Camel)
            int variable = 10;
        }
        public void MethodEventHandler()
        {
        }
        public void MethodCallback()
        {
        }
    }

    public interface IInterface
    {
        int Property { get; set; }

        void Method(int args);
    }

    enum Enum
    {
        Enum1,//枚舉值(Pascal)
        Enum2,//枚舉值(Pascal)
    }
}
```
-----

## 通用規則

所有的命名都是以下面兩種方式進行命名:
- PascalCasing(匈牙利命名法:每個詞的第一個字母大寫)
- camelCasing(駱駝命名法:第一個詞的第一個字母小寫,之後每個詞的第一個字母大寫)


類型|命名方式|範例
---|:--:|---:
Namespace|Pascal|`namespace System.Security { ... }`
Type|Pascal|`public class StreamReader { ... }`
Interface|Pascal|`public interface IEnumerable { ... }`
Method|Pascal|`public virtual string ToString();`
Property|Pascal|`public int Length { get; }`
Delegate|Pascal|`public delegate void EvnetHandler();`
Event|Pascal|`public event EventHandler Exited;`
Public Field|Pascal|`public readonly int Min = 0;`
Protected Field|Pascal|`protected int Min = 0;`
private Field|Camel|`private int _min = 0;`
Enum|Pascal|`public enum FileMode`
Parameter|Camel|`public static int ToInt32(string value)`
Local Variable|Camel|`void Method(){int number = 10;}`

-----

## 特殊說明

### 註解
**文件註解**: 文件註解以如下方式進行,在擴展註解時(換行時)會自動添加註釋行. 
  ```csharp
  /*
  * Description:This is a example class
  * Author: Hiram  
  * Date: 20220218 
  */
  ```

**其他註解**:以"//"的方式註解在上一行或行尾添加註解(比如局部變量,邏輯行)

**段落註解**:以如下方式進行
  ```csharp
  /*
  public void Method()
  {

  }
  */
  ```
**代辦項目**:在註解中添加"ToDo"顯示註解

-----
## **命名空間**
-----

## **常數**
>*	能用const就表示成const
>*	如果const 不能用考慮用readonly
>*	Magic number 定義成常數

----

## **類別**
>**類別內的順序**
>1. 巢狀類別
>2. Static, const 和 readonly 欄位
>3. 欄位和屬性
>4. Constructor和finalizers
>5. 方法
>6. 如果可以的話相同介面的實作放在一起

>**上述每個以下面規則排序**
>1. Public
>2. Internal
>3. Protected internal
>4. Protected
>5. Private

>**物件初始化**
>   ```csharp
>   var x = new SomeClass {
>       Property1 = value1,
>       Property2 = value2,
>   };
>   ```
-----

## **檔案**
>- 檔案名稱與類別或介面名稱一致
>- 一個檔案只放一個核心類別(不包含DTO、列舉或介面)
-----

## **常數**
>-	能用 const 就表示成 const
>-	如果 const 不能用考慮用 readonly
>-	Magic number 定義成常數
-----

## **結構**
>-	結構(Struct)命名方式與類一致
>-	用類別而不要用結構，除非你明白使用結構的好處
-----

## **泛型**
>-	泛型默認T
>-	多個泛型根據反編譯微軟dll, 建議命名 T1, T2, T3, T4...
-----

## **介面**
>-	介面定義以 "I" 開頭,比如 IInterface, IPlayer
>
-----

## **方法**
>-  #### Bad
>   ```csharp
>   // Bad - what are these arguments?
>   DecimalNumber product = CalculateProduct(values, 7, false, null);
>   ```
--------
>-  #### Good
>   ```csharp
>   // Good
>   ProductOptions options = new ProductOptions();
>   options.PrecisionDecimals = 7;
>   options.UseCache = CacheUsage.DontUseCache;
>   DecimalNumber product = CalculateProduct(values, options, completionDelegate: null);
>   ```
-----

## **屬性**
>-	屬性都以Pascal方式命名
>-	單行read-only的屬性用 => 
>   ```csharp
>   int SomeProperty => _someProperty
>   ```
-----

## **委托**
>-	微軟官方建議添加以下兩種後綴
>>-	EventHandler
>>-	Callback
>-  微軟官方不建議添加以下後綴
>> ~~Delegate~~
-----

## **事件**
>-	微軟官方建議:事件參數添加後綴 "EventArgs"
>-	習慣命名:事件以On為前綴 (比如 OnClick)
>-	使用 ? 調用, 例如 SomeDelegate?.Invoke()
-----

## **欄位**
>-	盡量給予初始值
>-	微軟團隊初期使用Camel方式命名私有欄位,後來逐步采用下劃線+Camel方式命名,這樣做的好處是不必再識別欄位是否是局部變量.
>-	欄位建議是非Public類型的,以屬性替換公有的字段:這樣外部訪問更安全(readonly,const等除外),並且外部調用者全部使用Pascal命名方式調用對象邏輯.
>-	微軟官方只規定了public/protected以Pascal方式命名,對internal,private類型的字段沒有說明,因此各種第三方規範和插件中對私有字段規範也不一致.
>-	針對官方的示例代碼,書寫習慣,智能提示,代碼補全和約定俗成的C#規範,建議private采用下劃線+Camel方式命名,非Private字段采用Pascal方式命名.

>-	參考微軟官方命名規則,C#高級編程,Resharp命名規範,.Net源碼命名規則,建議字段命名方式如下:
>>-	public/protected/internal以Pascal方式命名
>>-	private以下劃線+Camel方式命名
>>  ```csharp
>>  public readonly int Field1;
>>  public const int Field2 = 0;
>>
>>  internal readonly int Field3;
>>  internal const int Field4 = 0;
>>
>>  private int _field5;
>>  private static int _field6;
>>  private readonly int _field7;
>>  private const int _field8=0;
>>  ```
>- ~~以m_為前綴的方式不建議(C++命名方式)~~**
>- ~~以類型為前綴的方式不建議(比如bool類型的字段以b為前綴,枚舉以e為前綴)~~**
>- **~~以類型為後綴的方式不建議(比如單位列表unitList,直接取名為units)~~**

-----

## **局部變量**
>-  局部變量可以使用var自動聲明類型
>
-----

## **其他規範**
>**行數與長度**
>-	一行最多80個字，多的換行，除非無法換行
>-	一個方法大約40行，考慮是否可以在不損害結構的情況下分解

## **Unity場景規範**
>- 動靜分離
>- 物件命名以物件上最重要的元件為前綴底線"_"


類型|範例
---|---
Canvas|cav_CanvasName
Image|img_ImageName
RawImage|rawImg_RawImageName
Text|txt_TextName
Button|btn_ButtonName
DropDown|drop_DropName
Toggle|toggle_ToggleName
ScrollRect|scrollRect_ScrollName
InputField|input_InputName
Spine|spine_SpineName
